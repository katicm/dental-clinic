import React from "react";
import DoctorTable from "../../components/doctor/DoctorTable";

const DoctorPage = () => {
  return (
    <div style={{ marginTop: "90px" }}>
      <DoctorTable />
    </div>
  );
};
export default DoctorPage;
