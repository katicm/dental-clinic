import { put } from "redux-saga/effects";
import {
  setFetchedTreatment,
  setFetchedTreatment_Failure,
} from "./treatment.actions";
import axios from "axios";
import { auth } from "../root-saga";

export function* fetchTreatmentSaga() {
  try {
    const response = yield axios.get(
      `${process.env.REACT_APP_BASE_ANYPOINT}/treatment`,
      auth()
    );

    yield put(
      setFetchedTreatment(
        response.data.map((item) => ({ ...item, key: item.treatmentID }))
      )
    );
  } catch (error) {
    yield put(setFetchedTreatment_Failure(error));
  }
}
export function* deleteTreatmentSaga(action) {
  try {
    const response = yield axios.delete(
      `${process.env.REACT_APP_BASE_ANYPOINT}/treatment/${action.id}`,
      auth()
    );
    if (response.status === 204) {
      yield fetchTreatmentSaga();
    }
  } catch (error) {
    yield put(
      setFetchedTreatment_Failure(
        "Delete statment conflicted with reference constraint"
      )
    );
  }
}
export function* updateTreatmentSaga(action) {
  try {
    const response = yield axios.put(
      `${process.env.REACT_APP_BASE_ANYPOINT}/treatment/${action.item.treatmentID}`,
      action.item,
      auth()
    );
    if (response.status === 200) {
      yield fetchTreatmentSaga();
    }
  } catch (error) {
    yield put(setFetchedTreatment_Failure("Unsuccessful update"));
  }
}
export function* createTreatmentSaga(action) {
  try {
    const response = yield axios.post(
      `${process.env.REACT_APP_BASE_ANYPOINT}/treatment`,
      action.item,
      auth()
    );
    if (response.status === 201) {
      yield fetchTreatmentSaga();
    }
  } catch (error) {
    yield put(setFetchedTreatment_Failure("Unsuccessful post"));
  }
}
