import PatientActionTypes from "./patient.types";

const INITAL_STATE = {
  isLoading: true,
  error: null,
  data: [],
};

const patientReducer = (state = INITAL_STATE, action) => {
  switch (action.type) {
    case PatientActionTypes.FETCH_PATIENT_START: {
      return { data: [], error: null, isLoading: true };
    }
    case PatientActionTypes.DELETE_PATIENT_START:
    case PatientActionTypes.UPDATE_PATIENT_START:
    case PatientActionTypes.CREATE_PATIENT_START: {
      return { ...state, isLoading: true, error: null };
    }
    case PatientActionTypes.ADD_PATIENT_START: {
      return {
        ...state,
        error: null,
        data: [action.item, ...state.data.filter((item) => item.key !== 0)],
      };
    }
    case PatientActionTypes.CANCEL_CREATE_PATIENT: {
      return {
        ...state,
        error: null,
        data: state.data.filter((item) => item.key !== 0),
      };
    }
    case PatientActionTypes.SET_FETCHED_PATIENT: {
      return { data: action.payload, isLoading: false, error: null };
    }
    case PatientActionTypes.SET_FETCHED_PATIENT_FAILURE: {
      return { ...state, error: action.error, isLoading: false };
    }
    default:
      return state;
  }
};
export default patientReducer;
