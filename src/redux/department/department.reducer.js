import DepartmentActionTypes from "./department.types";

const INITAL_STATE = {
  isLoading: true,
  error: null,
  data: [],
};

const departmentReducer = (state = INITAL_STATE, action) => {
  switch (action.type) {
    case DepartmentActionTypes.FETCH_DEPARTMENT_START: {
      return { data: [], error: null, isLoading: true };
    }
    case DepartmentActionTypes.DELETE_DEPARTMENT_START:
    case DepartmentActionTypes.UPDATE_DEPARTMENT_START:
    case DepartmentActionTypes.CREATE_DEPARTMENT_START: {
      return { ...state, isLoading: true, error: null };
    }
    case DepartmentActionTypes.ADD_DEPARTMENT_START: {
      return {
        ...state,
        error: null,
        data: [action.item, ...state.data.filter((item) => item.key !== 0)],
      };
    }
    case DepartmentActionTypes.CANCEL_CREATE_DEPARTMENT: {
      return {
        ...state,
        error: null,
        data: state.data.filter((item) => item.key !== 0),
      };
    }
    case DepartmentActionTypes.SET_FETCHED_DEPARTMENT: {
      return { data: action.payload, isLoading: false, error: null };
    }
    case DepartmentActionTypes.SET_FETCHED_DEPARTMENT_FAILURE: {
      return { ...state, error: action.error, isLoading: false };
    }
    default:
      return state;
  }
};
export default departmentReducer;
