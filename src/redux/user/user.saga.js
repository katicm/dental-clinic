import { put } from "redux-saga/effects";
import { setFetchedUser, setFetchedUser_Failure } from "./user.actions";
import axios from "axios";
import { auth } from "../root-saga";

export function* fetchUserSaga() {
  try {
    const response = yield axios.get(
      `${process.env.REACT_APP_BASE_ANYPOINT}/user`,
      auth()
    );
    yield put(
      setFetchedUser(
        response.data.map((item) => ({ ...item, key: item.userID }))
      )
    );
  } catch (error) {
    yield put(setFetchedUser_Failure(error));
  }
}
export function* deleteUserSaga(action) {
  try {
    const response = yield axios.delete(
      `${process.env.REACT_APP_BASE_ANYPOINT}/user/${action.id}`,
      auth()
    );
    if (response.status === 204) {
      yield fetchUserSaga();
    }
  } catch (error) {
    yield put(
      setFetchedUser_Failure(
        "Delete statment conflicted with reference constraint"
      )
    );
  }
}
export function* updateUserSaga(action) {
  try {
    const response = yield axios.put(
      `${process.env.REACT_APP_BASE_ANYPOINT}/user/${action.item.userID}`,
      action.item,
      auth()
    );
    if (response.status === 200) {
      yield fetchUserSaga();
    }
  } catch (error) {
    yield put(setFetchedUser_Failure("Unsuccessful update"));
  }
}
export function* createUserSaga(action) {
  try {
    console.log(action.item);
    const response = yield axios.post(
      `${process.env.REACT_APP_BASE_ANYPOINT}/user`,
      action.item,
      auth()
    );
    if (response.status === 201) {
      yield fetchUserSaga();
    }
  } catch (error) {
    yield put(setFetchedUser_Failure("Unsuccessful post"));
  }
}
