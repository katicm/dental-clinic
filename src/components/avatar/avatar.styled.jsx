import styled from "styled-components";

const AvatarContainer = styled.div`
  color: white;
  font-family: Georgia, serif, sans-serif;
  font-size: 25px;
  float: right;
  position: relative;
  top: 20px;
  .avatar {
    top: -20px;
    margin-left: 20px;
  }
  .dropdown {
    top: -20px;
  }
`;
export default AvatarContainer;
