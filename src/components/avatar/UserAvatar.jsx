import React, { useEffect } from "react";
import { connect } from "react-redux";
import { setSignOut } from "../../redux/auth/auth.action";
import { Avatar } from "antd";
import AvatarContainer from "./avatar.styled";
import { UserOutlined, DownOutlined } from "@ant-design/icons";
import { Dropdown, Menu } from "antd";

const UserAvatar = ({ signOut, user }) => {
  useEffect(() => {
    if (!user) localStorage.clear();
  }, [user]);

  if (!user) return null;
  return (
    <AvatarContainer>
      <Dropdown
        placement="bottomRight"
        overlay={
          <Menu style={{ top: "-20px" }}>
            <Menu.Item onClick={signOut}>
              <b>Sign Out</b>
            </Menu.Item>
          </Menu>
        }
      >
        <div>
          <b>{user.account.username}</b> <DownOutlined />
          <Avatar className="avatar" size={64} icon={<UserOutlined />} />
        </div>
      </Dropdown>
    </AvatarContainer>
  );
};
const mapStateToProps = (state) => ({
  user: state.auth.user,
});
const mapDispatchToProps = (dispatch) => ({
  signOut: () => dispatch(setSignOut()),
});
export default connect(mapStateToProps, mapDispatchToProps)(UserAvatar);
