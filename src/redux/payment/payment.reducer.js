import PaymentActionTypes from "./payment.types";

const INITAL_STATE = {
  isLoading: true,
  error: null,
  data: [],
};

const paymentReducer = (state = INITAL_STATE, action) => {
  switch (action.type) {
    case PaymentActionTypes.FETCH_PAYMENT_START: {
      return { data: [], error: null, isLoading: true };
    }
    case PaymentActionTypes.DELETE_PAYMENT_START:
    case PaymentActionTypes.UPDATE_PAYMENT_START:
    case PaymentActionTypes.CREATE_PAYMENT_START: {
      return { ...state, isLoading: true, error: null };
    }
    case PaymentActionTypes.ADD_PAYMENT_START: {
      return {
        ...state,
        error: null,
        data: [action.item, ...state.data.filter((item) => item.key !== 0)],
      };
    }
    case PaymentActionTypes.CANCEL_CREATE_PAYMENT: {
      return {
        ...state,
        error: null,
        data: state.data.filter((item) => item.key !== 0),
      };
    }
    case PaymentActionTypes.SET_FETCHED_PAYMENT: {
      return { data: action.payload, isLoading: false, error: null };
    }
    case PaymentActionTypes.SET_FETCHED_PAYMENT_FAILURE: {
      return { ...state, error: action.error, isLoading: false };
    }
    default:
      return state;
  }
};
export default paymentReducer;
