import React, { useState, useEffect } from "react";
import { Form, Input, Button, Spin, Divider } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { signIn } from "../../redux/auth/auth.action";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { Avatar } from "antd";
import "./sign.style.scss";

const SignInForm = (props) => {
  const [form] = Form.useForm();
  const [redirect, setRedirect] = useState();
  const { signIn, auth } = props;
  const { user, isLoading, error } = auth;

  const onFinish = (data) => {
    signIn(data);
  };

  useEffect(() => {
    if (user) {
      /*localStorage.setItem(
        "basic",
        Buffer.from(
          `${user.account.email}:${user.account.password}`,
          "utf8"
        ).toString("base64")
      );*/
      setRedirect("/account");
    }
  }, [user]);

  useEffect(() => {
    if (error) {
      form.resetFields();
    }
  }, [error, form]);

  const handleSignUp = () => {
    setRedirect("/sign-up");
  };

  if (redirect) return <Redirect to={redirect} />;
  return (
    <div>
      <Spin spinning={isLoading} size="large">
        <Avatar
          style={{ background: "#40a9ff", marginBottom: "30px" }}
          size={84}
          icon={<UserOutlined />}
        />
        <Form
          form={form}
          name="normal_login"
          className="login-form"
          onFinish={onFinish}
        >
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                message: "Please input your Email!",
              },
            ]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Email"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your Password!",
              },
            ]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              LOG IN
            </Button>
          </Form.Item>
          <Divider>OR</Divider>
          <Form.Item>
            Don't have an account?
            <div className="link-element" onClick={handleSignUp}>
              Sign up
            </div>
          </Form.Item>
        </Form>
      </Spin>
    </div>
  );
};
const mapDispatchToProps = (dispatch) => ({
  signIn: (data) => dispatch(signIn(data)),
});
const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps, mapDispatchToProps)(SignInForm);
