import ServiceActionTypes from "./service.types";

export const fetchService = () => ({
  type: ServiceActionTypes.FETCH_SERVICE_START,
});

export const deleteService = (id) => ({
  type: ServiceActionTypes.DELETE_SERVICE_START,
  id,
});

export const updateService = (item) => ({
  type: ServiceActionTypes.UPDATE_SERVICE_START,
  item,
});

export const addService = (item) => ({
  type: ServiceActionTypes.ADD_SERVICE_START,
  item,
});

export const createService = (item) => ({
  type: ServiceActionTypes.CREATE_SERVICE_START,
  item,
});

export const cancelService = () => ({
  type: ServiceActionTypes.CANCEL_CREATE_SERVICE,
});

export const setFetchedService = (data) => ({
  type: ServiceActionTypes.SET_FETCHED_SERVICE,
  payload: data,
});

export const setFetchedService_Failure = (error) => ({
  type: ServiceActionTypes.SET_FETCHED_SERVICE_FAILURE,
  error,
});
