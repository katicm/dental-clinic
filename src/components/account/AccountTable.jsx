import React from "react";
import useEditableTable from "../custom-table/useEditableTable";
import useColumnSearch from "../custom-table/useColumnSearch";
import useOperationColumn from "../custom-table/useOperationColumn";
import Addable from "../custom-table/Addable";
import EditableTable from "../custom-table/EditableTable";
import moment from "moment";
import { connect } from "react-redux";
import {
  fetchAccount,
  deleteAccount,
  updateAccount,
  createAccount,
  cancelAccount,
  addAccount,
} from "../../redux/account/account.actions";
import "../custom-table/table.style.scss";

const AccountTable = (props) => {
  const {
    fetchAccount,
    deleteAccount,
    updateAccount,
    createAccount,
    cancelAccount,
    addAccount,
    account,
  } = props;

  const { isLoading } = account;

  const [getColumnSearchProps] = useColumnSearch();
  const [getEditDelete, getSaveCancel] = useOperationColumn();

  const handleAddable = () => {
    addAccount({
      key: 0,
      created: moment().format("YYYY-MM-DDTHH:mm:ss"),
    });
    handleAdd();
  };
  const columns = [
    {
      title: "Account ID",
      dataIndex: "accountID",
      width: "12%",
      sorter: (a, b) => a.accountID - b.accountID,
    },
    {
      title: <center>E-mail</center>,
      dataIndex: "email",
      editable: true,
      width: "14%",
      ...getColumnSearchProps("email"),
    },
    {
      title: <center>Username</center>,
      dataIndex: "username",
      editable: true,
      width: "10%",
      ...getColumnSearchProps("username"),
    },
    {
      title: <center>Role</center>,
      dataIndex: "role",
      editable: true,
      width: "10%",
      inputType: "selectRole",
      filters: [
        { text: "Admin", value: "Admin" },
        { text: "Guest", value: "Guest" },
        { text: "Doctor", value: "Doctor" },
      ],
      onFilter: (value, record) => record.role.indexOf(value) === 0,
    },
    {
      title: <center>Password</center>,
      dataIndex: "password",
      editable: true,
      width: "12%",
      sorter: (a, b) => {
        if (a.password < b.password) {
          return -1;
        } else if (a.password > b.password) {
          return 1;
        }
        return 0;
      },
    },
    {
      title: <center>Created</center>,
      dataIndex: "created",
      editable: true,
      width: "14%",
      inputType: "date",
      sorter: (a, b) => {
        if (a.created < b.created) {
          return -1;
        } else if (a.created > b.created) {
          return 1;
        }
        return 0;
      },
      render: (date) => date.replace("T", " "),
    },
    {
      title: <center>Phone</center>,
      dataIndex: "phone",
      editable: true,
      width: "10%",
    },
    {
      title: <center>Operation</center>,
      dataIndex: "operation",
      width: "12%",
      render: (_, record) => {
        const editable = record.key === editingKey ? true : false;
        return editable
          ? getSaveCancel(handleSave, handleCancel)
          : getEditDelete(editingKey, record, handleEdit, handleDelete);
      },
    },
  ];

  const [
    form,
    handleSave,
    handleCancel,
    handleEdit,
    handleDelete,
    handleAdd,
    editingKey,
    mergedColumns,
    handleChange,
    currentPage,
  ] = useEditableTable(
    columns,
    fetchAccount,
    deleteAccount,
    updateAccount,
    createAccount,
    cancelAccount,
    account
  );

  return (
    <React.Fragment>
      <Addable handleAddable={handleAddable} />
      <EditableTable
        form={form}
        isLoading={isLoading}
        data={account.data}
        columns={mergedColumns}
        onChange={handleChange}
        currentPage={currentPage}
      />
    </React.Fragment>
  );
};
const mapStateToProps = (state) => ({
  account: state.account,
});
const mapDispatchToProps = (dispatch) => ({
  fetchAccount: () => dispatch(fetchAccount()),
  deleteAccount: (id) => dispatch(deleteAccount(id)),
  updateAccount: (item) => dispatch(updateAccount(item)),
  createAccount: (item) => dispatch(createAccount(item)),
  cancelAccount: () => dispatch(cancelAccount()),
  addAccount: (nowDate) => dispatch(addAccount(nowDate)),
});
export default connect(mapStateToProps, mapDispatchToProps)(AccountTable);
