import React from "react";
import useEditableTable from "../custom-table/useEditableTable";
import useOperationColumn from "../custom-table/useOperationColumn";
import Addable from "../custom-table/Addable";
import EditableTable from "../custom-table/EditableTable";
import { Tag } from "antd";
import { connect } from "react-redux";
import {
  fetchPayment,
  deletePayment,
  updatePayment,
  createPayment,
  cancelPayment,
  addPayment,
} from "../../redux/payment/payment.actions";
import "../custom-table/table.style.scss";

const PaymentTable = (props) => {
  const {
    fetchPayment,
    deletePayment,
    updatePayment,
    createPayment,
    cancelPayment,
    addPayment,
    payment,
  } = props;

  const { isLoading } = payment;

  const [getEditDelete, getSaveCancel] = useOperationColumn();

  const handleAddable = () => {
    addPayment({
      key: 0,
      paid: false,
    });
    handleAdd();
  };

  const columns = [
    {
      title: "Payment ID",
      dataIndex: "paymentID",
      width: "12%",
      sorter: (a, b) => a.paymentID - b.paymentID,
    },
    {
      title: "Cost",
      dataIndex: "cost",
      width: "12%",
      sorter: (a, b) => a.cost - b.cost,
    },
    {
      title: <center>Paid</center>,
      dataIndex: "paid",
      width: "12%",
      editable: true,
      inputType: "selectPaid",
      filters: [
        { text: "Paid", value: true },
        { text: "Unpaid", value: false },
      ],
      render: (bit) =>
        bit ? <Tag color="success">Paid</Tag> : <Tag color="error">Unpaid</Tag>,
      onFilter: (value, record) => record.paid === value,
    },
    {
      title: <center>Paid Time</center>,
      dataIndex: "paidTime",
      width: "16%",
      inputType: "dateTime",
      sorter: (a, b) => {
        if (a.paidTime < b.paidTime) {
          return -1;
        } else if (a.paidTime > b.paidTime) {
          return 1;
        }
        return 0;
      },
      render: (date) => date && date.replace("T", " "),
    },
    {
      title: <center>Operation</center>,
      dataIndex: "operation",
      width: "10%",
      render: (_, record) => {
        const editable = record.key === editingKey ? true : false;
        return editable
          ? getSaveCancel(handleSave, handleCancel)
          : getEditDelete(editingKey, record, handleEdit, handleDelete);
      },
    },
  ];

  const [
    form,
    handleSave,
    handleCancel,
    handleEdit,
    handleDelete,
    handleAdd,
    editingKey,
    mergedColumns,
    handleChange,
    currentPage,
  ] = useEditableTable(
    columns,
    fetchPayment,
    deletePayment,
    updatePayment,
    createPayment,
    cancelPayment,
    payment
  );
  return (
    <React.Fragment>
      <Addable handleAddable={handleAddable} />
      <EditableTable
        form={form}
        isLoading={isLoading}
        data={payment.data}
        columns={mergedColumns}
        onChange={handleChange}
        currentPage={currentPage}
      />
    </React.Fragment>
  );
};
const mapStateToProps = (state) => ({
  payment: state.payment,
});
const mapDispatchToProps = (dispatch) => ({
  fetchPayment: () => dispatch(fetchPayment()),
  deletePayment: (id) => dispatch(deletePayment(id)),
  updatePayment: (item) => dispatch(updatePayment(item)),
  createPayment: (item) => dispatch(createPayment(item)),
  cancelPayment: () => dispatch(cancelPayment()),
  addPayment: (nowDate) => dispatch(addPayment(nowDate)),
});
export default connect(mapStateToProps, mapDispatchToProps)(PaymentTable);
