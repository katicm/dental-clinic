import { createStore, applyMiddleware } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import createSagaMiddleware from "redux-saga";
import rootReducer from "./root-reducer";
import {
  watchAccount,
  watchUser,
  watchAuth,
  watchPatient,
  watchService,
  watchDepartment,
  watchDoctor,
  watchAppointment,
  watchTreatment,
  watchVisit,
  watchPayment,
} from "./root-saga";

const sagaMiddleware = createSagaMiddleware();

const middlewares = [thunk, sagaMiddleware];

if (process.env.NODE_ENV === "development") {
  middlewares.push(logger);
}

const store = createStore(rootReducer, applyMiddleware(...middlewares));

sagaMiddleware.run(watchAccount);
sagaMiddleware.run(watchUser);
sagaMiddleware.run(watchAuth);
sagaMiddleware.run(watchPatient);
sagaMiddleware.run(watchService);
sagaMiddleware.run(watchDepartment);
sagaMiddleware.run(watchDoctor);
sagaMiddleware.run(watchAppointment);
sagaMiddleware.run(watchTreatment);
sagaMiddleware.run(watchVisit);
sagaMiddleware.run(watchPayment);

export default store;
