import React from "react";
import TreatmentTable from "../../components/treatment/TreatmentTable";

const TreatmentPage = () => {
  return (
    <div style={{ marginTop: "90px" }}>
      <TreatmentTable />
    </div>
  );
};
export default TreatmentPage;
