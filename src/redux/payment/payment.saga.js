import { put } from "redux-saga/effects";
import {
  setFetchedPayment,
  setFetchedPayment_Failure,
} from "./payment.actions";
import axios from "axios";
import { auth } from "../root-saga";

export function* fetchPaymentSaga() {
  try {
    const response = yield axios.get(
      `${process.env.REACT_APP_BASE_ANYPOINT}/payment`,
      auth()
    );
    yield put(
      setFetchedPayment(
        response.data.map((item) => ({ ...item, key: item.paymentID }))
      )
    );
  } catch (error) {
    yield put(setFetchedPayment_Failure(error));
  }
}
export function* deletePaymentSaga(action) {
  try {
    const response = yield axios.delete(
      `${process.env.REACT_APP_BASE_ANYPOINT}/payment/${action.id}`,
      auth()
    );
    if (response.status === 204) {
      yield fetchPaymentSaga();
    }
  } catch (error) {
    yield put(
      setFetchedPayment_Failure(
        "Delete statment conflicted with reference constraint"
      )
    );
  }
}
export function* updatePaymentSaga(action) {
  try {
    const response = yield axios.put(
      `${process.env.REACT_APP_BASE_ANYPOINT}/payment/${action.item.paymentID}`,
      action.item,
      auth()
    );
    if (response.status === 200) {
      yield fetchPaymentSaga();
    }
  } catch (error) {
    yield put(setFetchedPayment_Failure("Unsuccessful update"));
  }
}
export function* createPaymentSaga(action) {
  try {
    const response = yield axios.post(
      `${process.env.REACT_APP_BASE_ANYPOINT}/payment`,
      action.item,
      auth()
    );
    if (response.status === 201) {
      yield fetchPaymentSaga();
    }
  } catch (error) {
    yield put(setFetchedPayment_Failure("Unsuccessful post"));
  }
}
