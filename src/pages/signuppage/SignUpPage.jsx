import React from "react";
import SignUpForm from "../../components/sign/SignUpForm";

const SignUpPage = () => {
  return (
    <div style={{ marginTop: "90px" }}>
      <SignUpForm />
    </div>
  );
};
export default SignUpPage;
