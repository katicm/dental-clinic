import { put } from "redux-saga/effects";
import {
  setFetchedAccount,
  setFetchedAccount_Failure,
} from "./account.actions";
import axios from "axios";
import { auth } from "../root-saga";

export function* fetchAccountSaga() {
  try {
    const response = yield axios.get(
      `${process.env.REACT_APP_BASE_ANYPOINT}/account`
    );
    if (response.status === 200)
      yield put(
        setFetchedAccount(
          response.data.map((item) => ({ ...item, key: item.accountID }))
        )
      );
  } catch (error) {
    yield put(setFetchedAccount_Failure(error));
  }
}
export function* deleteAccountSaga(action) {
  try {
    const response = yield axios.delete(
      `${process.env.REACT_APP_BASE_ANYPOINT}/account/${action.id}`,
      auth()
    );
    if (response.status === 204) {
      yield fetchAccountSaga();
    }
  } catch (error) {
    yield put(
      setFetchedAccount_Failure(
        "Delete statment conflicted with reference constraint"
      )
    );
  }
}
export function* updateAccountSaga(action) {
  try {
    const response = yield axios.put(
      `${process.env.REACT_APP_BASE_ANYPOINT}/account`,
      action.item
    );
    if (response.status === 200) {
      yield fetchAccountSaga();
    }
  } catch (error) {
    yield put(setFetchedAccount_Failure("Unsuccessful update"));
  }
}
export function* createAccountSaga(action) {
  try {
    const response = yield axios.post(
      `${process.env.REACT_APP_BASE_ANYPOINT}/account`,
      action.item,
      auth()
    );
    if (response.status === 201) {
      yield fetchAccountSaga();
    }
  } catch (error) {
    yield put(setFetchedAccount_Failure("Unsuccessful post"));
  }
}
