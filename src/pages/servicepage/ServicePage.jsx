import React from "react";
import ServiceTable from "../../components/service/ServiceTable";

const ServicePage = () => {
  return (
    <div style={{ marginTop: "90px" }}>
      <ServiceTable />
    </div>
  );
};
export default ServicePage;
