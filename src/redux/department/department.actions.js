import DepartmentActionTypes from "./department.types";

export const fetchDepartment = () => ({
  type: DepartmentActionTypes.FETCH_DEPARTMENT_START,
});

export const deleteDepartment = (id) => ({
  type: DepartmentActionTypes.DELETE_DEPARTMENT_START,
  id,
});

export const updateDepartment = (item) => ({
  type: DepartmentActionTypes.UPDATE_DEPARTMENT_START,
  item,
});

export const addDepartment = (item) => ({
  type: DepartmentActionTypes.ADD_DEPARTMENT_START,
  item,
});

export const createDepartment = (item) => ({
  type: DepartmentActionTypes.CREATE_DEPARTMENT_START,
  item,
});

export const cancelDepartment = () => ({
  type: DepartmentActionTypes.CANCEL_CREATE_DEPARTMENT,
});

export const setFetchedDepartment = (data) => ({
  type: DepartmentActionTypes.SET_FETCHED_DEPARTMENT,
  payload: data,
});

export const setFetchedDepartment_Failure = (error) => ({
  type: DepartmentActionTypes.SET_FETCHED_DEPARTMENT_FAILURE,
  error,
});
