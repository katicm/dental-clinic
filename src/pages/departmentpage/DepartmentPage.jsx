import React from "react";
import DepartmentTable from "../../components/department/DepartmentTable";

const DepartmentPage = () => {
  return (
    <div style={{ marginTop: "90px" }}>
      <DepartmentTable />
    </div>
  );
};
export default DepartmentPage;
