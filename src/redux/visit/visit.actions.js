import VisitActionTypes from "./visit.types";

export const fetchVisit = () => ({
  type: VisitActionTypes.FETCH_VISIT_START,
});

export const deleteVisit = (id) => ({
  type: VisitActionTypes.DELETE_VISIT_START,
  id,
});

export const updateVisit = (item) => ({
  type: VisitActionTypes.UPDATE_VISIT_START,
  item,
});

export const addVisit = (item) => ({
  type: VisitActionTypes.ADD_VISIT_START,
  item,
});

export const createVisit = (item) => ({
  type: VisitActionTypes.CREATE_VISIT_START,
  item,
});

export const cancelVisit = () => ({
  type: VisitActionTypes.CANCEL_CREATE_VISIT,
});

export const setFetchedVisit = (data) => ({
  type: VisitActionTypes.SET_FETCHED_VISIT,
  payload: data,
});

export const setFetchedVisit_Failure = (error) => ({
  type: VisitActionTypes.SET_FETCHED_VISIT_FAILURE,
  error,
});
