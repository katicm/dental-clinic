import React, { useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Layout, Menu } from "antd";
import {
  PieChartOutlined,
  TeamOutlined,
  HeatMapOutlined,
  RadarChartOutlined,
  WeiboSquareOutlined,
  TrademarkOutlined,
  SmileOutlined,
  CodepenCircleOutlined,
  MediumOutlined,
  DribbbleOutlined,
} from "@ant-design/icons";
import "./slider.style.scss";
import logo_big from "../../media/2panda.png";
import logo_small from "../../media/1panda.png";

const { Sider } = Layout;
const SliderMenu = ({ user }) => {
  const [collapsed, setCollapsed] = useState(false);

  return (
    <React.Fragment>
      {user && (
        <Sider
          className="slider-container"
          collapsible
          collapsed={collapsed}
          onCollapse={() => setCollapsed(!collapsed)}
        >
          <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
            <div className="slider-logo">
              {collapsed ? (
                <React.Fragment>
                  <img src={logo_small} alt="P" className="small-logo" />
                  <img src={logo_small} alt="P" className="small-logo" />
                </React.Fragment>
              ) : (
                <img src={logo_big} alt="2Panda" className="big-logo" />
              )}
            </div>
            <Menu.Item key="1" icon={<PieChartOutlined />}>
              <Link to="/account">Account</Link>
            </Menu.Item>
            <Menu.Item key="2" icon={<TeamOutlined />}>
              <Link to="/user">User</Link>
            </Menu.Item>
            <Menu.Item key="3" icon={<HeatMapOutlined />}>
              <Link to="/patient">Patient</Link>
            </Menu.Item>
            <Menu.Item key="4" icon={<RadarChartOutlined />}>
              <Link to="/doctor">Doctor</Link>
            </Menu.Item>
            <Menu.Item key="5" icon={<WeiboSquareOutlined />}>
              <Link to="/appointment">Appointment</Link>
            </Menu.Item>
            <Menu.Item key="6" icon={<TrademarkOutlined />}>
              <Link to="/treatment">Treatment</Link>
            </Menu.Item>
            <Menu.Item key="7" icon={<SmileOutlined />}>
              <Link to="/service">Service</Link>
            </Menu.Item>
            <Menu.Item key="8" icon={<CodepenCircleOutlined />}>
              <Link to="/visit">Visit</Link>
            </Menu.Item>
            <Menu.Item key="9" icon={<DribbbleOutlined />}>
              <Link to="/payment">Payment</Link>
            </Menu.Item>
            <Menu.Item key="10" icon={<MediumOutlined />}>
              <Link to="/department">Department</Link>
            </Menu.Item>
          </Menu>
        </Sider>
      )}
    </React.Fragment>
  );
};
const mapStateToProps = (state) => ({
  user: state.auth.user,
});
export default connect(mapStateToProps)(SliderMenu);
