import React from "react";
import SignInForm from "../../components/sign/SignInForm";

const SignInPage = () => {
  return (
    <div style={{ marginTop: "90px" }}>
      <SignInForm />
    </div>
  );
};
export default SignInPage;
