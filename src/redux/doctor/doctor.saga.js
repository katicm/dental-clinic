import { put } from "redux-saga/effects";
import { setFetchedDoctor, setFetchedDoctor_Failure } from "./doctor.actions";
import axios from "axios";
import { auth } from "../root-saga";

export function* fetchDoctorSaga() {
  try {
    const response = yield axios.get(
      `${process.env.REACT_APP_BASE_ANYPOINT}/doctor`,
      auth()
    );
    yield put(
      setFetchedDoctor(
        response.data.map((item) => ({ ...item, key: item.doctorID }))
      )
    );
  } catch (error) {
    yield put(setFetchedDoctor_Failure(error));
  }
}
export function* deleteDoctorSaga(action) {
  try {
    const response = yield axios.delete(
      `${process.env.REACT_APP_BASE_ANYPOINT}/doctor/${action.id}`,
      auth()
    );
    if (response.status === 204) {
      yield fetchDoctorSaga();
    }
  } catch (error) {
    yield put(
      setFetchedDoctor_Failure(
        "Delete statment conflicted with reference constraint"
      )
    );
  }
}
export function* updateDoctorSaga(action) {
  try {
    const response = yield axios.put(
      `${process.env.REACT_APP_BASE_ANYPOINT}/doctor`,
      action.item
    );
    if (response.status === 200) {
      yield fetchDoctorSaga();
    }
  } catch (error) {
    yield put(setFetchedDoctor_Failure("Unsuccessful update"));
  }
}
export function* createDoctorSaga(action) {
  try {
    const response = yield axios.post(
      `${process.env.REACT_APP_BASE_ANYPOINT}/doctor`,
      action.item,
      auth()
    );
    if (response.status === 201) {
      yield fetchDoctorSaga();
    }
  } catch (error) {
    yield put(setFetchedDoctor_Failure("Unsuccessful post"));
  }
}
