import React from "react";
import AppointmentTable from "../../components/appointment/AppointmentTable";

const AppointmentPage = () => {
  return (
    <div style={{ marginTop: "90px" }}>
      <AppointmentTable />
    </div>
  );
};
export default AppointmentPage;
