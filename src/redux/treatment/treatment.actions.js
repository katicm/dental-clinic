import TreatmentActionTypes from "./treatment.types";

export const fetchTreatment = () => ({
  type: TreatmentActionTypes.FETCH_TREATMENT_START,
});

export const deleteTreatment = (id) => ({
  type: TreatmentActionTypes.DELETE_TREATMENT_START,
  id,
});

export const updateTreatment = (item) => ({
  type: TreatmentActionTypes.UPDATE_TREATMENT_START,
  item,
});

export const addTreatment = (item) => ({
  type: TreatmentActionTypes.ADD_TREATMENT_START,
  item,
});

export const createTreatment = (item) => ({
  type: TreatmentActionTypes.CREATE_TREATMENT_START,
  item,
});

export const cancelTreatment = () => ({
  type: TreatmentActionTypes.CANCEL_CREATE_TREATMENT,
});

export const setFetchedTreatment = (data) => ({
  type: TreatmentActionTypes.SET_FETCHED_TREATMENT,
  payload: data,
});

export const setFetchedTreatment_Failure = (error) => ({
  type: TreatmentActionTypes.SET_FETCHED_TREATMENT_FAILURE,
  error,
});
