import React from "react";
import PaymentTable from "../../components/payment/PaymentTable";

const PaymentPage = () => {
  return (
    <div style={{ marginTop: "90px" }}>
      <PaymentTable />
    </div>
  );
};
export default PaymentPage;
