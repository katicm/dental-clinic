import React from "react";
import VisitTable from "../../components/visit/VisitTable";

const VisitPage = () => {
  return (
    <div style={{ marginTop: "90px" }}>
      <VisitTable />
    </div>
  );
};
export default VisitPage;
