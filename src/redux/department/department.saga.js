import { put } from "redux-saga/effects";
import {
  setFetchedDepartment,
  setFetchedDepartment_Failure,
} from "./department.actions";
import axios from "axios";
import { auth } from "../root-saga";

export function* fetchDepartmentSaga() {
  try {
    const response = yield axios.get(
      `${process.env.REACT_APP_BASE_ANYPOINT}/department`,
      auth()
    );
    yield put(
      setFetchedDepartment(
        response.data.map((item) => ({ ...item, key: item.departmentID }))
      )
    );
  } catch (error) {
    yield put(setFetchedDepartment_Failure(error));
  }
}
export function* deleteDepartmentSaga(action) {
  try {
    const response = yield axios.delete(
      `${process.env.REACT_APP_BASE_ANYPOINT}/department/${action.id}`,
      auth()
    );
    if (response.status === 204) {
      yield fetchDepartmentSaga();
    }
  } catch (error) {
    yield put(
      setFetchedDepartment_Failure(
        "Delete statment conflicted with reference constraint"
      )
    );
  }
}
export function* updateDepartmentSaga(action) {
  try {
    const response = yield axios.put(
      `${process.env.REACT_APP_BASE_ANYPOINT}/department/${action.item.departmentID}`,
      action.item,
      auth()
    );
    if (response.status === 200) {
      yield fetchDepartmentSaga();
    }
  } catch (error) {
    yield put(setFetchedDepartment_Failure("Unsuccessful update"));
  }
}
export function* createDepartmentSaga(action) {
  try {
    const response = yield axios.post(
      `${process.env.REACT_APP_BASE_ANYPOINT}/department`,
      action.item,
      auth()
    );
    if (response.status === 201) {
      yield fetchDepartmentSaga();
    }
  } catch (error) {
    yield put(setFetchedDepartment_Failure("Unsuccessful post"));
  }
}
