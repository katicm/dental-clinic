import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import AccountPage from "./pages/accountpage/AccountPage";
import UserPage from "./pages/userpage/UserPage";
import PatientPage from "./pages/patientpage/PatientPage";
import ServicePage from "./pages/servicepage/ServicePage";
import DepartmentPage from "./pages/departmentpage/DepartmentPage";
import DoctorPage from "./pages/doctorpage/DoctorPage";
import AppointmentPage from "./pages/appointmentpage/AppointmentPage";
import TreatmentPage from "./pages/treatmentpage/TreatmentPage";
import VisitPage from "./pages/visitpage/VisitPage";
import PaymentPage from "./pages/paymentpage/PaymentPage";
import "./App.scss";
import "antd/dist/antd.css";
import SignInPage from "./pages/signinpage/SignInPage";
import SignUpPage from "./pages/signuppage/SignUpPage";
import SliderMenu from "./components/slider/SliderMenu";
import UserAvatar from "./components/avatar/UserAvatar";
import AuthorizedRoute from "./pages/authorizedpage/AuthorizedRoute";
import { Layout } from "antd";

const { Header, Content } = Layout;

function App() {
  return (
    <div className="App">
      <Layout className="main-container">
        <SliderMenu />
        <Layout>
          <Header className="header-style">
            <UserAvatar />
          </Header>
          <Content className="center-container">
            <Switch>
              <Route exact path="/" render={() => <Redirect to="/sign-in" />} />
              <Route exact path="/sign-in" component={SignInPage} />
              <Route exact path="/sign-up" component={SignUpPage} />
              <AuthorizedRoute exact path="/account" component={AccountPage} />
              <AuthorizedRoute exact path="/user" component={UserPage} />
              <AuthorizedRoute exact path="/patient" component={PatientPage} />
              <AuthorizedRoute exact path="/service" component={ServicePage} />
              <AuthorizedRoute
                exact
                path="/department"
                component={DepartmentPage}
              />
              <AuthorizedRoute exact path="/doctor" component={DoctorPage} />
              <AuthorizedRoute
                exact
                path="/treatment"
                component={TreatmentPage}
              />
              <AuthorizedRoute exact path="/visit" component={VisitPage} />
              <AuthorizedRoute exact path="/payment" component={PaymentPage} />
              <AuthorizedRoute
                exact
                path="/appointment"
                component={AppointmentPage}
              />
            </Switch>
          </Content>
          <div className="footer-style">2Panda Design ©2020</div>
        </Layout>
      </Layout>
    </div>
  );
}
export default App;
