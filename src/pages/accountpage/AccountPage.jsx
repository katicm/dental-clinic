import React from "react";
import AccountTable from "../../components/account/AccountTable";

const AccountPage = () => {
  return (
    <div style={{ marginTop: "90px" }}>
      <AccountTable />
    </div>
  );
};
export default AccountPage;
