import React from "react";
import { Button } from "antd";

const Addable = ({ handleAddable }) => {
  return (
    <div className="add_button">
      <Button onClick={handleAddable} type="primary">
        Insert New Row
      </Button>
    </div>
  );
};
export default Addable;
