import React, { useState } from "react";
import { Input, Form, DatePicker, InputNumber, Select } from "antd";
import moment from "moment";
import "./table.style.scss";
const { Option } = Select;

const EditableCell = ({
  editing,
  dataIndex,
  inputType,
  children,
  form,
  ...restProps
}) => {
  const [state, setState] = useState(children[1]);

  const handleChange = (data) => {
    setState(data);
  };

  const props = {
    style: { margin: "0" },
    rules: [{ required: true, message: `Required field!` }],
  };

  const inputNode = (inputType) => {
    switch (inputType) {
      case "date": {
        form.setFieldsValue({ [dataIndex]: state });
        return (
          <Form.Item
            {...props}
            valuePropName={moment(state, "YYYY-MM-DD")}
            name={dataIndex}
          >
            <DatePicker
              onChange={(_, data) => handleChange(data)}
              value={moment(state, "YYYY-MM-DD")}
            />
          </Form.Item>
        );
      }
      case "dateTime": {
        form.setFieldsValue({ [dataIndex]: state });
        return (
          <Form.Item {...props} valuePropName={moment(state)} name={dataIndex}>
            <DatePicker
              onChange={(_, data) => handleChange(data)}
              showTime
              value={moment(state)}
            />
          </Form.Item>
        );
      }
      case "number": {
        return (
          <Form.Item name={dataIndex} {...props}>
            <InputNumber placeholder={dataIndex} />
          </Form.Item>
        );
      }
      case "selectGender": {
        return (
          <Form.Item name={dataIndex} {...props}>
            <Select placeholder="gender" value={state}>
              <Option value="Male">Male</Option>
              <Option value="Female">Female</Option>
              <Option value="Other">Other</Option>
            </Select>
          </Form.Item>
        );
      }
      case "selectRole": {
        return (
          <Form.Item name={dataIndex} {...props}>
            <Select placeholder="role" value={state}>
              <Option value="Doctor">Doctor</Option>
              <Option value="Guest">Guest</Option>
              <Option value="Admin">Admin</Option>
            </Select>
          </Form.Item>
        );
      }
      case "selectPaid": {
        return (
          <Form.Item name={dataIndex} {...props}>
            <Select placeholder="paid" value={state}>
              <Option value="1">Paid</Option>
              <Option value="0">Unpaid</Option>
            </Select>
          </Form.Item>
        );
      }
      default: {
        return (
          <Form.Item name={dataIndex} {...props}>
            <Input placeholder={dataIndex} className="form-input" />
          </Form.Item>
        );
      }
    }
  };
  return (
    <td {...restProps} className="td-edit">
      {editing ? inputNode(inputType) : children}
    </td>
  );
};
export default EditableCell;
