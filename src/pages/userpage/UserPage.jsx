import React from "react";
import UserTable from "../../components/user/UserTable";

const UserPage = () => {
  return (
    <div style={{ marginTop: "10px" }}>
      <UserTable />
    </div>
  );
};
export default UserPage;
