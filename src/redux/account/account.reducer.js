import AccountActionTypes from "./account.types";

const INITAL_STATE = {
  isLoading: true,
  error: null,
  data: [],
};

const accountReducer = (state = INITAL_STATE, action) => {
  switch (action.type) {
    case AccountActionTypes.FETCH_ACCOUNT_START: {
      return { data: [], error: null, isLoading: true };
    }
    case AccountActionTypes.DELETE_ACCOUNT_START:
    case AccountActionTypes.UPDATE_ACCOUNT_START:
    case AccountActionTypes.CREATE_ACCOUNT_START: {
      return { ...state, isLoading: true, error: null };
    }
    case AccountActionTypes.ADD_ACCOUNT_START: {
      return {
        ...state,
        error: null,
        data: [action.item, ...state.data.filter((item) => item.key !== 0)],
      };
    }
    case AccountActionTypes.CANCEL_CREATE_ACCOUNT: {
      return {
        ...state,
        error: null,
        data: state.data.filter((item) => item.key !== 0),
      };
    }
    case AccountActionTypes.SET_FETCHED_ACCOUNT: {
      return { data: action.payload, isLoading: false, error: null };
    }
    case AccountActionTypes.SET_FETCHED_ACCOUNT_FAILURE: {
      return { ...state, error: action.error, isLoading: false };
    }
    default:
      return state;
  }
};
export default accountReducer;
