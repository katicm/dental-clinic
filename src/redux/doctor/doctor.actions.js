import DoctorActionTypes from "./doctor.types";

export const fetchDoctor = () => ({
  type: DoctorActionTypes.FETCH_DOCTOR_START,
});

export const deleteDoctor = (id) => ({
  type: DoctorActionTypes.DELETE_DOCTOR_START,
  id,
});

export const updateDoctor = (item) => ({
  type: DoctorActionTypes.UPDATE_DOCTOR_START,
  item,
});

export const addDoctor = (item) => ({
  type: DoctorActionTypes.ADD_DOCTOR_START,
  item,
});

export const createDoctor = (item) => ({
  type: DoctorActionTypes.CREATE_DOCTOR_START,
  item,
});

export const cancelDoctor = () => ({
  type: DoctorActionTypes.CANCEL_CREATE_DOCTOR,
});

export const setFetchedDoctor = (data) => ({
  type: DoctorActionTypes.SET_FETCHED_DOCTOR,
  payload: data,
});

export const setFetchedDoctor_Failure = (error) => ({
  type: DoctorActionTypes.SET_FETCHED_DOCTOR_FAILURE,
  error,
});
