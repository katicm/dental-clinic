import React from "react";
import useEditableTable from "../custom-table/useEditableTable";
import useColumnSearch from "../custom-table/useColumnSearch";
import useOperationColumn from "../custom-table/useOperationColumn";
import Addable from "../custom-table/Addable";
import EditableTable from "../custom-table/EditableTable";
import { connect } from "react-redux";
import {
  fetchDepartment,
  deleteDepartment,
  updateDepartment,
  createDepartment,
  cancelDepartment,
  addDepartment,
} from "../../redux/department/department.actions";
import "../custom-table/table.style.scss";

const DepartmentTable = (props) => {
  const {
    fetchDepartment,
    deleteDepartment,
    updateDepartment,
    createDepartment,
    cancelDepartment,
    addDepartment,
    department,
  } = props;

  const { isLoading } = department;

  const [getColumnSearchProps] = useColumnSearch();
  const [getEditDelete, getSaveCancel] = useOperationColumn();

  const handleAddable = () => {
    addDepartment({
      key: 0,
    });
    handleAdd();
  };

  const columns = [
    {
      title: "Department ID",
      dataIndex: "departmentID",
      width: "5%",
      sorter: (a, b) => a.departmentID - b.departmentID,
    },
    {
      title: <center>Name of department</center>,
      dataIndex: "name",
      editable: true,
      width: "10%",
      ...getColumnSearchProps("name"),
    },
    {
      title: <center>Operation</center>,
      dataIndex: "operation",
      width: "20%",
      render: (_, record) => {
        const editable = record.key === editingKey ? true : false;
        return editable
          ? getSaveCancel(handleSave, handleCancel)
          : getEditDelete(editingKey, record, handleEdit, handleDelete);
      },
    },
  ];

  const [
    form,
    handleSave,
    handleCancel,
    handleEdit,
    handleDelete,
    handleAdd,
    editingKey,
    mergedColumns,
    handleChange,
    currentPage,
  ] = useEditableTable(
    columns,
    fetchDepartment,
    deleteDepartment,
    updateDepartment,
    createDepartment,
    cancelDepartment,
    department
  );
  return (
    <React.Fragment>
      <Addable handleAddable={handleAddable} />
      <EditableTable
        form={form}
        isLoading={isLoading}
        data={department.data}
        columns={mergedColumns}
        onChange={handleChange}
        currentPage={currentPage}
      />
    </React.Fragment>
  );
};
const mapStateToProps = (state) => ({
  department: state.department,
});
const mapDispatchToProps = (dispatch) => ({
  fetchDepartment: () => dispatch(fetchDepartment()),
  deleteDepartment: (id) => dispatch(deleteDepartment(id)),
  updateDepartment: (item) => dispatch(updateDepartment(item)),
  createDepartment: (item) => dispatch(createDepartment(item)),
  cancelDepartment: () => dispatch(cancelDepartment()),
  addDepartment: (nowDate) => dispatch(addDepartment(nowDate)),
});
export default connect(mapStateToProps, mapDispatchToProps)(DepartmentTable);
