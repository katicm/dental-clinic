import { put } from "redux-saga/effects";
import {
  setFetchedPatient,
  setFetchedPatient_Failure,
} from "./patient.actions";
import axios from "axios";
import { auth } from "../root-saga";

export function* fetchPatientSaga() {
  try {
    const response = yield axios.get(
      `${process.env.REACT_APP_BASE_ANYPOINT}/patient`,
      auth()
    );
    yield put(
      setFetchedPatient(
        response.data.map((item) => ({ ...item, key: item.patientID }))
      )
    );
  } catch (error) {
    yield put(setFetchedPatient_Failure(error));
  }
}
export function* deletePatientSaga(action) {
  try {
    const response = yield axios.delete(
      `${process.env.REACT_APP_BASE_ANYPOINT}/patient/${action.id}`,
      auth()
    );
    if (response.status === 204) {
      yield fetchPatientSaga();
    }
  } catch (error) {
    yield put(
      setFetchedPatient_Failure(
        "Delete statment conflicted with reference constraint"
      )
    );
  }
}
export function* updatePatientSaga(action) {
  try {
    const response = yield axios.put(
      `${process.env.REACT_APP_BASE_ANYPOINT}/patient/${action.item.patientID}`,
      action.item,
      auth()
    );
    if (response.status === 200) {
      yield fetchPatientSaga();
    }
  } catch (error) {
    yield put(setFetchedPatient_Failure("Unsuccessful update"));
  }
}
export function* createPatientSaga(action) {
  try {
    const response = yield axios.post(
      `${process.env.REACT_APP_BASE_ANYPOINT}/patient`,
      action.item,
      auth()
    );
    if (response.status === 201) {
      yield fetchPatientSaga();
    }
  } catch (error) {
    yield put(setFetchedPatient_Failure("Unsuccessful post"));
  }
}
