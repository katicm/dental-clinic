import UserActionTypes from "./user.types";

const INITAL_STATE = {
  isLoading: true,
  error: null,
  data: [],
};

const userReducer = (state = INITAL_STATE, action) => {
  switch (action.type) {
    case UserActionTypes.FETCH_USER_START: {
      return { data: [], error: null, isLoading: true };
    }
    case UserActionTypes.DELETE_USER_START:
    case UserActionTypes.UPDATE_USER_START:
    case UserActionTypes.CREATE_USER_START: {
      return { ...state, isLoading: true, error: null };
    }
    case UserActionTypes.ADD_USER_START: {
      return {
        ...state,
        data: [action.item, ...state.data.filter((item) => item.key !== 0)],
      };
    }
    case UserActionTypes.CANCEL_CREATE_USER: {
      return {
        ...state,
        data: state.data.filter((item) => item.key !== 0),
      };
    }
    case UserActionTypes.SET_FETCHED_USER: {
      return { data: action.payload, isLoading: false, error: null };
    }
    case UserActionTypes.SET_FETCHED_USER_FAILURE: {
      return { ...state, error: action.error, isLoading: false };
    }
    default:
      return state;
  }
};
export default userReducer;
