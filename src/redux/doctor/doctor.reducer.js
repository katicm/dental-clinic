import DoctorActionTypes from "./doctor.types";

const INITAL_STATE = {
  isLoading: true,
  error: null,
  data: [],
};

const doctorReducer = (state = INITAL_STATE, action) => {
  switch (action.type) {
    case DoctorActionTypes.FETCH_DOCTOR_START: {
      return { data: [], error: null, isLoading: true };
    }
    case DoctorActionTypes.DELETE_DOCTOR_START:
    case DoctorActionTypes.UPDATE_DOCTOR_START:
    case DoctorActionTypes.CREATE_DOCTOR_START: {
      return { ...state, isLoading: true, error: null };
    }
    case DoctorActionTypes.ADD_DOCTOR_START: {
      return {
        ...state,
        error: null,
        data: [action.item, ...state.data.filter((item) => item.key !== 0)],
      };
    }
    case DoctorActionTypes.CANCEL_CREATE_DOCTOR: {
      return {
        ...state,
        error: null,
        data: state.data.filter((item) => item.key !== 0),
      };
    }
    case DoctorActionTypes.SET_FETCHED_DOCTOR: {
      return { data: action.payload, isLoading: false, error: null };
    }
    case DoctorActionTypes.SET_FETCHED_DOCTOR_FAILURE: {
      return { ...state, error: action.error, isLoading: false };
    }
    default:
      return state;
  }
};
export default doctorReducer;
