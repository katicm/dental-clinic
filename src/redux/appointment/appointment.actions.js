import AppointmentActionTypes from "./appointment.types";

export const fetchAppointment = () => ({
  type: AppointmentActionTypes.FETCH_APPOINTMENT_START,
});

export const deleteAppointment = (id) => ({
  type: AppointmentActionTypes.DELETE_APPOINTMENT_START,
  id,
});

export const updateAppointment = (item) => ({
  type: AppointmentActionTypes.UPDATE_APPOINTMENT_START,
  item,
});

export const addAppointment = (item) => ({
  type: AppointmentActionTypes.ADD_APPOINTMENT_START,
  item,
});

export const createAppointment = (item) => ({
  type: AppointmentActionTypes.CREATE_APPOINTMENT_START,
  item,
});

export const cancelAppointment = () => ({
  type: AppointmentActionTypes.CANCEL_CREATE_APPOINTMENT,
});

export const setFetchedAppointment = (data) => ({
  type: AppointmentActionTypes.SET_FETCHED_APPOINTMENT,
  payload: data,
});

export const setFetchedAppointment_Failure = (error) => ({
  type: AppointmentActionTypes.SET_FETCHED_APPOINTMENT_FAILURE,
  error,
});
