import React from "react";
import useEditableTable from "../custom-table/useEditableTable";
import useColumnSearch from "../custom-table/useColumnSearch";
import useOperationColumn from "../custom-table/useOperationColumn";
import Addable from "../custom-table/Addable";
import EditableTable from "../custom-table/EditableTable";
import { connect } from "react-redux";
import {
  fetchDoctor,
  deleteDoctor,
  updateDoctor,
  createDoctor,
  cancelDoctor,
  addDoctor,
} from "../../redux/doctor/doctor.actions";
import "../custom-table/table.style.scss";

const DoctorTable = (props) => {
  const {
    fetchDoctor,
    deleteDoctor,
    updateDoctor,
    createDoctor,
    cancelDoctor,
    addDoctor,
    doctor,
  } = props;

  const { isLoading } = doctor;

  const [getColumnSearchProps] = useColumnSearch();
  const [getEditDelete, getSaveCancel] = useOperationColumn();

  const handleAddable = () => {
    addDoctor({
      key: 0,
    });
    handleAdd();
  };

  const columns = [
    {
      title: "Doctor ID",
      dataIndex: "doctorID",
      width: "15%",
      sorter: (a, b) => a.doctorID - b.doctorID,
    },
    {
      title: "User ID",
      dataIndex: "userID",
      inputType: "number",
      editable: true,
      width: "15%",
      sorter: (a, b) => a.userID - b.userID,
    },
    {
      title: "Department ID",
      dataIndex: "departmentID",
      inputType: "number",
      editable: true,
      width: "15%",
      sorter: (a, b) => a.departmentID - b.departmentID,
    },
    {
      title: <center>Doctor Title</center>,
      dataIndex: "title",
      editable: true,
      width: "15%",
      ...getColumnSearchProps("title"),
    },
    {
      title: <center>Operation</center>,
      dataIndex: "operation",
      width: "10%",
      render: (_, record) => {
        const editable = record.key === editingKey ? true : false;
        return editable
          ? getSaveCancel(handleSave, handleCancel)
          : getEditDelete(editingKey, record, handleEdit, handleDelete);
      },
    },
  ];

  const [
    form,
    handleSave,
    handleCancel,
    handleEdit,
    handleDelete,
    handleAdd,
    editingKey,
    mergedColumns,
    handleChange,
    currentPage,
  ] = useEditableTable(
    columns,
    fetchDoctor,
    deleteDoctor,
    updateDoctor,
    createDoctor,
    cancelDoctor,
    doctor
  );
  return (
    <React.Fragment>
      <Addable handleAddable={handleAddable} />
      <EditableTable
        form={form}
        isLoading={isLoading}
        data={doctor.data}
        columns={mergedColumns}
        onChange={handleChange}
        currentPage={currentPage}
      />
    </React.Fragment>
  );
};
const mapStateToProps = (state) => ({
  doctor: state.doctor,
});
const mapDispatchToProps = (dispatch) => ({
  fetchDoctor: () => dispatch(fetchDoctor()),
  deleteDoctor: (id) => dispatch(deleteDoctor(id)),
  updateDoctor: (item) => dispatch(updateDoctor(item)),
  createDoctor: (item) => dispatch(createDoctor(item)),
  cancelDoctor: () => dispatch(cancelDoctor()),
  addDoctor: (nowDate) => dispatch(addDoctor(nowDate)),
});
export default connect(mapStateToProps, mapDispatchToProps)(DoctorTable);
