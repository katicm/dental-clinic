import React from "react";
import useEditableTable from "../custom-table/useEditableTable";
import useColumnSearch from "../custom-table/useColumnSearch";
import useOperationColumn from "../custom-table/useOperationColumn";
import Addable from "../custom-table/Addable";
import EditableTable from "../custom-table/EditableTable";
import { connect } from "react-redux";
import {
  fetchService,
  deleteService,
  updateService,
  createService,
  cancelService,
  addService,
} from "../../redux/service/service.actions";
import "../custom-table/table.style.scss";

const ServiceTable = (props) => {
  const {
    fetchService,
    deleteService,
    updateService,
    createService,
    cancelService,
    addService,
    service,
  } = props;

  const { isLoading } = service;

  const [getColumnSearchProps] = useColumnSearch();
  const [getEditDelete, getSaveCancel] = useOperationColumn();

  const handleAddable = () => {
    addService({
      key: 0,
    });
    handleAdd();
  };

  const columns = [
    {
      title: "Service ID",
      dataIndex: "serviceID",
      width: "12%",
      sorter: (a, b) => a.serviceID - b.serviceID,
    },
    {
      title: <center>Name of service</center>,
      dataIndex: "name",
      editable: true,
      width: "12%",
      ...getColumnSearchProps("name"),
    },
    {
      title: <center>Cost [$]</center>,
      dataIndex: "cost",
      inputType: "number",
      editable: true,
      width: "15%",
      sorter: (a, b) => a.cost - b.cost,
    },
    {
      title: <center>Duration [min]</center>,
      dataIndex: "duration",
      inputType: "number",
      editable: true,
      width: "15%",
      sorter: (a, b) => a.duration - b.duration,
    },
    {
      title: <center>Operation</center>,
      dataIndex: "operation",
      width: "15%",
      render: (_, record) => {
        const editable = record.key === editingKey ? true : false;
        return editable
          ? getSaveCancel(handleSave, handleCancel)
          : getEditDelete(editingKey, record, handleEdit, handleDelete);
      },
    },
  ];

  const [
    form,
    handleSave,
    handleCancel,
    handleEdit,
    handleDelete,
    handleAdd,
    editingKey,
    mergedColumns,
    handleChange,
    currentPage,
  ] = useEditableTable(
    columns,
    fetchService,
    deleteService,
    updateService,
    createService,
    cancelService,
    service
  );
  return (
    <React.Fragment>
      <Addable handleAddable={handleAddable} />
      <EditableTable
        form={form}
        isLoading={isLoading}
        data={service.data}
        columns={mergedColumns}
        onChange={handleChange}
        currentPage={currentPage}
      />
    </React.Fragment>
  );
};
const mapStateToProps = (state) => ({
  service: state.service,
});
const mapDispatchToProps = (dispatch) => ({
  fetchService: () => dispatch(fetchService()),
  deleteService: (id) => dispatch(deleteService(id)),
  updateService: (item) => dispatch(updateService(item)),
  createService: (item) => dispatch(createService(item)),
  cancelService: () => dispatch(cancelService()),
  addService: (nowDate) => dispatch(addService(nowDate)),
});
export default connect(mapStateToProps, mapDispatchToProps)(ServiceTable);
