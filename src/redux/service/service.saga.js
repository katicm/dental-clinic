import { put } from "redux-saga/effects";
import {
  setFetchedService,
  setFetchedService_Failure,
} from "./service.actions";
import axios from "axios";
import { auth } from "../root-saga";

export function* fetchServiceSaga() {
  try {
    const response = yield axios.get(
      `${process.env.REACT_APP_BASE_ANYPOINT}/service`,
      auth()
    );
    yield put(
      setFetchedService(
        response.data.map((item) => ({ ...item, key: item.serviceID }))
      )
    );
  } catch (error) {
    yield put(setFetchedService_Failure(error));
  }
}
export function* deleteServiceSaga(action) {
  try {
    const response = yield axios.delete(
      `${process.env.REACT_APP_BASE_ANYPOINT}/service/${action.id}`,
      auth()
    );
    if (response.status === 204) {
      yield fetchServiceSaga();
    }
  } catch (error) {
    yield put(
      setFetchedService_Failure(
        "Delete statment conflicted with reference constraint"
      )
    );
  }
}
export function* updateServiceSaga(action) {
  try {
    const response = yield axios.put(
      `${process.env.REACT_APP_BASE_ANYPOINT}/service/${action.item.serviceID}`,
      action.item,
      auth()
    );
    if (response.status === 200) {
      yield fetchServiceSaga();
    }
  } catch (error) {
    yield put(setFetchedService_Failure("Unsuccessful update"));
  }
}
export function* createServiceSaga(action) {
  try {
    const response = yield axios.post(
      `${process.env.REACT_APP_BASE_ANYPOINT}/service`,
      action.item,
      auth()
    );
    if (response.status === 201) {
      yield fetchServiceSaga();
    }
  } catch (error) {
    yield put(setFetchedService_Failure("Unsuccessful post"));
  }
}
