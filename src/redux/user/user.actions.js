import UserActionTypes from "./user.types";

export const fetchUser = () => ({
  type: UserActionTypes.FETCH_USER_START,
});

export const deleteUser = (id) => ({
  type: UserActionTypes.DELETE_USER_START,
  id,
});

export const updateUser = (item) => ({
  type: UserActionTypes.UPDATE_USER_START,
  item,
});

export const addUser = (item) => ({
  type: UserActionTypes.ADD_USER_START,
  item,
});

export const createUser = (item) => ({
  type: UserActionTypes.CREATE_USER_START,
  item,
});

export const cancelUser = () => ({
  type: UserActionTypes.CANCEL_CREATE_USER,
});

export const setFetchedUser = (data) => ({
  type: UserActionTypes.SET_FETCHED_USER,
  payload: data,
});

export const setFetchedUser_Failure = (error) => ({
  type: UserActionTypes.SET_FETCHED_USER_FAILURE,
  error,
});
