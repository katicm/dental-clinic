import React from "react";
import useEditableTable from "../custom-table/useEditableTable";
import useColumnSearch from "../custom-table/useColumnSearch";
import useOperationColumn from "../custom-table/useOperationColumn";
import Addable from "../custom-table/Addable";
import EditableTable from "../custom-table/EditableTable";
import moment from "moment";
import { connect } from "react-redux";
import {
  fetchPatient,
  deletePatient,
  updatePatient,
  createPatient,
  cancelPatient,
  addPatient,
} from "../../redux/patient/patient.actions";
import "../custom-table/table.style.scss";

const PatientTable = (props) => {
  const {
    fetchPatient,
    deletePatient,
    updatePatient,
    createPatient,
    cancelPatient,
    addPatient,
    patient,
  } = props;

  const { isLoading } = patient;

  const [getColumnSearchProps] = useColumnSearch();
  const [getEditDelete, getSaveCancel] = useOperationColumn();

  const handleAddable = () => {
    addPatient({
      key: 0,
      firstVisit: moment().format("YYYY-MM-DDTHH:mm:ss"),
      lastVisit: moment().format("YYYY-MM-DDTHH:mm:ss"),
    });
    handleAdd();
  };

  const columns = [
    {
      title: "Patient ID",
      dataIndex: "patientID",
      width: "10%",
      sorter: (a, b) => a.patientID - b.patientID,
    },
    {
      title: <center>User ID</center>,
      dataIndex: "userID",
      inputType: "number",
      editable: true,
      width: "8%",
      sorter: (a, b) => a.userID - b.userID,
    },
    {
      title: <center>Blood Group</center>,
      dataIndex: "bloodGroup",
      editable: true,
      width: "10%",
      ...getColumnSearchProps("bloodGroup"),
    },
    {
      title: <center>Notes</center>,
      dataIndex: "notes",
      editable: true,
      width: "12%",
      ...getColumnSearchProps("notes"),
    },
    {
      title: <center>First Visit</center>,
      dataIndex: "firstVisit",
      editable: true,
      width: "16%",
      inputType: "dateTime",
      sorter: (a, b) => {
        if (a.firstVisit < b.firstVisit) {
          return -1;
        } else if (a.firstVisit > b.firstVisit) {
          return 1;
        }
        return 0;
      },
      render: (date) => date.replace("T", " "),
    },
    {
      title: <center>Last Visit</center>,
      dataIndex: "lastVisit",
      editable: true,
      width: "16%",
      inputType: "dateTime",
      sorter: (a, b) => {
        if (a.lastVisit < b.lastVisit) {
          return -1;
        } else if (a.lastVisit > b.lastVisit) {
          return 1;
        }
        return 0;
      },
      render: (date) => date.replace("T", " "),
    },
    {
      title: <center>Allergy</center>,
      dataIndex: "allergy",
      editable: true,
      width: "11%",
      ...getColumnSearchProps("allergy"),
    },
    {
      title: <center>Visit Count</center>,
      dataIndex: "visitCount",
      inputType: "number",
      editable: false,
      width: "8%",
      sorter: (a, b) => a.visitCount - b.visitCount,
    },
    {
      title: <center>Operation</center>,
      dataIndex: "operation",
      width: "14%",
      render: (_, record) => {
        const editable = record.key === editingKey ? true : false;
        return editable
          ? getSaveCancel(handleSave, handleCancel)
          : getEditDelete(editingKey, record, handleEdit, handleDelete);
      },
    },
  ];

  const [
    form,
    handleSave,
    handleCancel,
    handleEdit,
    handleDelete,
    handleAdd,
    editingKey,
    mergedColumns,
    handleChange,
    currentPage,
  ] = useEditableTable(
    columns,
    fetchPatient,
    deletePatient,
    updatePatient,
    createPatient,
    cancelPatient,
    patient
  );
  return (
    <React.Fragment>
      <Addable handleAddable={handleAddable} />
      <EditableTable
        form={form}
        isLoading={isLoading}
        data={patient.data}
        columns={mergedColumns}
        onChange={handleChange}
        currentPage={currentPage}
      />
    </React.Fragment>
  );
};
const mapStateToProps = (state) => ({
  patient: state.patient,
});
const mapDispatchToProps = (dispatch) => ({
  fetchPatient: () => dispatch(fetchPatient()),
  deletePatient: (id) => dispatch(deletePatient(id)),
  updatePatient: (item) => dispatch(updatePatient(item)),
  createPatient: (item) => dispatch(createPatient(item)),
  cancelPatient: () => dispatch(cancelPatient()),
  addPatient: (nowDate) => dispatch(addPatient(nowDate)),
});
export default connect(mapStateToProps, mapDispatchToProps)(PatientTable);
