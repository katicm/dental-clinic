import AccountActionTypes from "./account.types";

export const fetchAccount = () => ({
  type: AccountActionTypes.FETCH_ACCOUNT_START,
});

export const deleteAccount = (id) => ({
  type: AccountActionTypes.DELETE_ACCOUNT_START,
  id,
});

export const updateAccount = (item) => ({
  type: AccountActionTypes.UPDATE_ACCOUNT_START,
  item,
});

export const addAccount = (item) => ({
  type: AccountActionTypes.ADD_ACCOUNT_START,
  item,
});

export const createAccount = (item) => ({
  type: AccountActionTypes.CREATE_ACCOUNT_START,
  item,
});

export const cancelAccount = () => ({
  type: AccountActionTypes.CANCEL_CREATE_ACCOUNT,
});

export const setFetchedAccount = (data) => ({
  type: AccountActionTypes.SET_FETCHED_ACCOUNT,
  payload: data,
});

export const setFetchedAccount_Failure = (error) => ({
  type: AccountActionTypes.SET_FETCHED_ACCOUNT_FAILURE,
  error,
});
