import React from "react";
import { Form, Table } from "antd";
import EditableCell from "./EditableCell";
import "./table.style.scss";

const EditableTable = ({
  form,
  isLoading,
  data,
  columns,
  onChange,
  currentPage,
}) => {
  return (
    <Form form={form} component={false}>
      <Table
        components={{
          body: {
            cell: EditableCell,
          },
        }}
        bordered
        loading={isLoading}
        dataSource={data}
        columns={columns}
        rowClassName="editable-row"
        pagination={{
          onChange: onChange,
          position: ["bottomCenter"],
          current: currentPage,
        }}
      />
    </Form>
  );
};
export default EditableTable;
