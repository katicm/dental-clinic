import React from "react";
import PatientTable from "../../components/patient/PatientTable";

const PatientPage = () => {
  return (
    <div style={{ marginTop: "90px" }}>
      <PatientTable />
    </div>
  );
};
export default PatientPage;
