import VisitActionTypes from "./visit.types";

const INITAL_STATE = {
  isLoading: true,
  error: null,
  data: [],
};

const visitReducer = (state = INITAL_STATE, action) => {
  switch (action.type) {
    case VisitActionTypes.FETCH_VISIT_START: {
      return { data: [], error: null, isLoading: true };
    }
    case VisitActionTypes.DELETE_VISIT_START:
    case VisitActionTypes.UPDATE_VISIT_START:
    case VisitActionTypes.CREATE_VISIT_START: {
      return { ...state, isLoading: true, error: null };
    }
    case VisitActionTypes.ADD_VISIT_START: {
      return {
        ...state,
        error: null,
        data: [action.item, ...state.data.filter((item) => item.key !== 0)],
      };
    }
    case VisitActionTypes.CANCEL_CREATE_VISIT: {
      return {
        ...state,
        error: null,
        data: state.data.filter((item) => item.key !== 0),
      };
    }
    case VisitActionTypes.SET_FETCHED_VISIT: {
      return { data: action.payload, isLoading: false, error: null };
    }
    case VisitActionTypes.SET_FETCHED_VISIT_FAILURE: {
      return { ...state, error: action.error, isLoading: false };
    }
    default:
      return state;
  }
};
export default visitReducer;
