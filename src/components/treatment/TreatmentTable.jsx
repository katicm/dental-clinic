import React from "react";
import useEditableTable from "../custom-table/useEditableTable";
import useColumnSearch from "../custom-table/useColumnSearch";
import useOperationColumn from "../custom-table/useOperationColumn";
import Addable from "../custom-table/Addable";
import { Tag } from "antd";
import EditableTable from "../custom-table/EditableTable";
import { connect } from "react-redux";
import {
  fetchTreatment,
  deleteTreatment,
  updateTreatment,
  createTreatment,
  cancelTreatment,
  addTreatment,
} from "../../redux/treatment/treatment.actions";
import "../custom-table/table.style.scss";

const TreatmentTable = (props) => {
  const {
    fetchTreatment,
    deleteTreatment,
    updateTreatment,
    createTreatment,
    cancelTreatment,
    addTreatment,
    treatment,
  } = props;

  const { isLoading } = props.treatment;

  const [getColumnSearchProps] = useColumnSearch();
  const [getEditDelete, getSaveCancel] = useOperationColumn();

  const handleAddable = () => {
    addTreatment({
      key: 0,
    });
    handleAdd();
  };

  const columns = [
    {
      title: "Treatment ID",
      dataIndex: "treatmentID",
      width: "10%",
      sorter: (a, b) => a.treatmentID - b.treatmentID,
    },
    {
      title: "Doctor ID",
      dataIndex: "doctorID",
      inputType: "number",
      editable: true,
      width: "10%",
      sorter: (a, b) => a.doctorID - b.doctorID,
    },
    {
      title: "Patient ID",
      dataIndex: "patientID",
      inputType: "number",
      editable: true,
      width: "10%",
      sorter: (a, b) => a.patientID - b.patientID,
    },
    {
      title: "Service ID",
      dataIndex: "serviceID",
      inputType: "number",
      editable: true,
      width: "10%",
      sorter: (a, b) => a.serviceID - b.serviceID,
    },
    {
      title: <center>Completed</center>,
      dataIndex: "completed",
      width: "8%",
      editable: true,
      inputType: "selectCompleted",
      filters: [
        { text: "Completed", value: true },
        { text: "Uncompleted", value: false },
      ],
      render: (bit) =>
        bit ? (
          <Tag color="success">Completed</Tag>
        ) : (
          <Tag color="error">Uncompleted</Tag>
        ),
      onFilter: (value, record) => record.completed === value,
    },
    {
      title: <center>Scheduled Time</center>,
      dataIndex: "scheduledTime",
      editable: true,
      width: "12%",
      inputType: "dateTime",
      //render: (date) => date.replace("T", " "),
    },
    {
      title: <center>Tooth No</center>,
      dataIndex: "toothNo",
      editable: true,
      width: "8%",
      ...getColumnSearchProps("toothNo"),
    },
    {
      title: <center>Type</center>,
      dataIndex: "type",
      editable: true,
      width: "8%",
      ...getColumnSearchProps("type"),
    },
    {
      title: <center>Zone</center>,
      dataIndex: "zone",
      editable: true,
      width: "8%",
      ...getColumnSearchProps("zone"),
    },
    {
      title: <center>Operation</center>,
      dataIndex: "operation",
      width: "15%",
      render: (_, record) => {
        const editable = record.key === editingKey ? true : false;
        return editable
          ? getSaveCancel(handleSave, handleCancel)
          : getEditDelete(editingKey, record, handleEdit, handleDelete);
      },
    },
  ];

  const [
    form,
    handleSave,
    handleCancel,
    handleEdit,
    handleDelete,
    handleAdd,
    editingKey,
    mergedColumns,
    handleChange,
    currentPage,
  ] = useEditableTable(
    columns,
    fetchTreatment,
    deleteTreatment,
    updateTreatment,
    createTreatment,
    cancelTreatment,
    treatment
  );
  return (
    <React.Fragment>
      <Addable handleAddable={handleAddable} />
      <EditableTable
        form={form}
        isLoading={isLoading}
        data={treatment.data}
        columns={mergedColumns}
        onChange={handleChange}
        currentPage={currentPage}
      />
    </React.Fragment>
  );
};
const mapStateToProps = (state) => ({
  treatment: state.treatment,
});
const mapDispatchToProps = (dispatch) => ({
  fetchTreatment: () => dispatch(fetchTreatment()),
  deleteTreatment: (id) => dispatch(deleteTreatment(id)),
  updateTreatment: (item) => dispatch(updateTreatment(item)),
  createTreatment: (item) => dispatch(createTreatment(item)),
  cancelTreatment: () => dispatch(cancelTreatment()),
  addTreatment: (nowDate) => dispatch(addTreatment(nowDate)),
});
export default connect(mapStateToProps, mapDispatchToProps)(TreatmentTable);
