const AppointmentActionTypes = {
  FETCH_APPOINTMENT_START: "FETCH_APPOINTMENT_START",
  SET_FETCHED_APPOINTMENT: "SET_FETCHED_APPOINTMENT",
  SET_FETCHED_APPOINTMENT_FAILURE: "SET_FETCHED_APPOINTMENT_FAILURE",
  UPDATE_APPOINTMENT_START: "UPDATE_APPOINTMENT_START",
  DELETE_APPOINTMENT_START: "DELETE_APPOINTMENT_START",
  CREATE_APPOINTMENT_START: "CREATE_APPOINTMENT_START",
  ADD_APPOINTMENT_START: "ADD_APPOINTMENT_START",
  CANCEL_CREATE_APPOINTMENT: "CANCEL_CREATE_APPOINTMENT",
};
export default AppointmentActionTypes;
