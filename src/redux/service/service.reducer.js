import ServiceActionTypes from "./service.types";

const INITAL_STATE = {
  isLoading: true,
  error: null,
  data: [],
};

const serviceReducer = (state = INITAL_STATE, action) => {
  switch (action.type) {
    case ServiceActionTypes.FETCH_SERVICE_START: {
      return { data: [], error: null, isLoading: true };
    }
    case ServiceActionTypes.DELETE_SERVICE_START:
    case ServiceActionTypes.UPDATE_SERVICE_START:
    case ServiceActionTypes.CREATE_SERVICE_START: {
      return { ...state, isLoading: true, error: null };
    }
    case ServiceActionTypes.ADD_SERVICE_START: {
      return {
        ...state,
        error: null,
        data: [action.item, ...state.data.filter((item) => item.key !== 0)],
      };
    }
    case ServiceActionTypes.CANCEL_CREATE_SERVICE: {
      return {
        ...state,
        error: null,
        data: state.data.filter((item) => item.key !== 0),
      };
    }
    case ServiceActionTypes.SET_FETCHED_SERVICE: {
      return { data: action.payload, isLoading: false, error: null };
    }
    case ServiceActionTypes.SET_FETCHED_SERVICE_FAILURE: {
      return { ...state, error: action.error, isLoading: false };
    }
    default:
      return state;
  }
};
export default serviceReducer;
