import PatientActionTypes from "./patient.types";

export const fetchPatient = () => ({
  type: PatientActionTypes.FETCH_PATIENT_START,
});

export const deletePatient = (id) => ({
  type: PatientActionTypes.DELETE_PATIENT_START,
  id,
});

export const updatePatient = (item) => ({
  type: PatientActionTypes.UPDATE_PATIENT_START,
  item,
});

export const addPatient = (item) => ({
  type: PatientActionTypes.ADD_PATIENT_START,
  item,
});

export const createPatient = (item) => ({
  type: PatientActionTypes.CREATE_PATIENT_START,
  item,
});

export const cancelPatient = () => ({
  type: PatientActionTypes.CANCEL_CREATE_PATIENT,
});

export const setFetchedPatient = (data) => ({
  type: PatientActionTypes.SET_FETCHED_PATIENT,
  payload: data,
});

export const setFetchedPatient_Failure = (error) => ({
  type: PatientActionTypes.SET_FETCHED_PATIENT_FAILURE,
  error,
});
