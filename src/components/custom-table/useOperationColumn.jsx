import React from "react";
import { Popconfirm } from "antd";
import {
  DeleteTwoTone,
  EditTwoTone,
  SaveTwoTone,
  CloseCircleTwoTone,
} from "@ant-design/icons";

const useOperationColumn = () => {
  const getEditDelete = (editingKey, record, handleEdit, handleDelete) => (
    <span>
      <button
        disabled={editingKey !== ""}
        className={editingKey !== "" ? "link-button-disable" : "link-button"}
        onClick={() => handleEdit(record)}
      >
        Edit
        <EditTwoTone />
      </button>
      <Popconfirm
        title="Are you sure you want to delete?"
        onConfirm={() => handleDelete(record.key)}
        disabled={editingKey !== ""}
      >
        <button
          className={editingKey !== "" ? "link-button-disable" : "link-button"}
        >
          Delete
          <DeleteTwoTone />
        </button>
      </Popconfirm>
    </span>
  );

  const getSaveCancel = (handleSave, handleCancel) => (
    <span>
      <button onClick={handleSave} className="link-button">
        Save
        <SaveTwoTone />
      </button>
      <button className="link-button" onClick={handleCancel}>
        Cancel
        <CloseCircleTwoTone />
      </button>
    </span>
  );

  return [getEditDelete, getSaveCancel];
};
export default useOperationColumn;
