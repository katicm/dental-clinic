import React from "react";
import useEditableTable from "../custom-table/useEditableTable";
import useColumnSearch from "../custom-table/useColumnSearch";
import useOperationColumn from "../custom-table/useOperationColumn";
import Addable from "../custom-table/Addable";
import EditableTable from "../custom-table/EditableTable";
import { connect } from "react-redux";
import {
  fetchUser,
  deleteUser,
  updateUser,
  createUser,
  cancelUser,
  addUser,
} from "../../redux/user/user.actions";
import "../custom-table/table.style.scss";

const UserTable = (props) => {
  const {
    fetchUser,
    deleteUser,
    updateUser,
    createUser,
    cancelUser,
    addUser,
    user,
  } = props;

  const { isLoading } = user;

  const [getColumnSearchProps] = useColumnSearch();
  const [getEditDelete, getSaveCancel] = useOperationColumn();

  const handleAddable = () => {
    addUser({ key: 0, birthDate: "1970-01-01" });
    handleAdd();
  };
  const columns = [
    {
      title: "User ID",
      dataIndex: "userID",
      width: "10%",
      sorter: (a, b) => a.userID - b.userID,
    },
    {
      title: <center>Account ID</center>,
      dataIndex: "accountID",
      inputType: "number",
      editable: true,
      width: "10%",
      sorter: (a, b) => a.accountID - b.accountID,
    },
    {
      title: <center>First Name</center>,
      dataIndex: "firstName",
      editable: true,
      width: "10%",
      ...getColumnSearchProps("firstName"),
    },
    {
      title: <center>Middle Name</center>,
      dataIndex: "middleName",
      editable: true,
      width: "10%",
      ...getColumnSearchProps("middleName"),
    },
    {
      title: <center>Last Name</center>,
      dataIndex: "lastName",
      editable: true,
      width: "10%",
      ...getColumnSearchProps("lastName"),
    },
    {
      title: <center>Gender</center>,
      dataIndex: "gender",
      editable: true,
      inputType: "selectGender",
      width: "10%",
      filters: [
        { text: "Female", value: "Female" },
        { text: "Male", value: "Male" },
        { text: "Other", value: "Other" },
      ],
      onFilter: (value, record) => record.gender.indexOf(value) === 0,
    },
    {
      title: <center>Birth Data</center>,
      dataIndex: "birthDate",
      editable: true,
      inputType: "date",
      width: "12%",
      sorter: (a, b) => {
        if (a.birthDate < b.birthDate) {
          return -1;
        } else if (a.birthDate > b.birthDate) {
          return 1;
        }
        return 0;
      },
      render: (date) => date.split("T")[0],
    },
    {
      title: <center>Address</center>,
      dataIndex: "address",
      editable: true,
      width: "15%",
      ...getColumnSearchProps("address"),
    },
    {
      title: <center>Operation</center>,
      dataIndex: "operation",
      width: "12%",
      render: (_, record) => {
        const editable = record.key === editingKey ? true : false;
        return editable
          ? getSaveCancel(handleSave, handleCancel)
          : getEditDelete(editingKey, record, handleEdit, handleDelete);
      },
    },
  ];

  const [
    form,
    handleSave,
    handleCancel,
    handleEdit,
    handleDelete,
    handleAdd,
    editingKey,
    mergedColumns,
    handleChange,
    currentPage,
  ] = useEditableTable(
    columns,
    fetchUser,
    deleteUser,
    updateUser,
    createUser,
    cancelUser,
    user
  );
  return (
    <React.Fragment>
      <Addable handleAddable={handleAddable} />
      <EditableTable
        form={form}
        isLoading={isLoading}
        data={user.data}
        columns={mergedColumns}
        onChange={handleChange}
        currentPage={currentPage}
      />
    </React.Fragment>
  );
};
const mapStateToProps = (state) => ({
  user: state.user,
});
const mapDispatchToProps = (dispatch) => ({
  fetchUser: () => dispatch(fetchUser()),
  deleteUser: (id) => dispatch(deleteUser(id)),
  updateUser: (item) => dispatch(updateUser(item)),
  createUser: (item) => dispatch(createUser(item)),
  cancelUser: () => dispatch(cancelUser()),
  addUser: (nowDate) => dispatch(addUser(nowDate)),
});
export default connect(mapStateToProps, mapDispatchToProps)(UserTable);
