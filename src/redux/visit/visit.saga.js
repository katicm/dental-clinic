import { put } from "redux-saga/effects";
import { setFetchedVisit, setFetchedVisit_Failure } from "./visit.actions";
import axios from "axios";
import { auth } from "../root-saga";

export function* fetchVisitSaga() {
  try {
    const response = yield axios.get(
      `${process.env.REACT_APP_BASE_ANYPOINT}/visit`,
      auth()
    );

    yield put(
      setFetchedVisit(
        response.data.map((item) => ({ ...item, key: item.visitID }))
      )
    );
  } catch (error) {
    yield put(setFetchedVisit_Failure(error));
  }
}
export function* deleteVisitSaga(action) {
  try {
    const response = yield axios.delete(
      `${process.env.REACT_APP_BASE_ANYPOINT}/visit/${action.id}`,
      auth()
    );
    if (response.status === 204) {
      yield fetchVisitSaga();
    }
  } catch (error) {
    yield put(
      setFetchedVisit_Failure(
        "Delete statment conflicted with reference constraint"
      )
    );
  }
}
export function* updateVisitSaga(action) {
  try {
    const response = yield axios.put(
      `${process.env.REACT_APP_BASE_ANYPOINT}/visit/${action.item.visitID}`,
      action.item,
      auth()
    );
    if (response.status === 200) {
      yield fetchVisitSaga();
    }
  } catch (error) {
    yield put(setFetchedVisit_Failure("Unsuccessful update"));
  }
}
export function* createVisitSaga(action) {
  try {
    const response = yield axios.post(
      `${process.env.REACT_APP_BASE_ANYPOINT}/visit`,
      action.item,
      auth()
    );
    if (response.status === 201) {
      yield fetchVisitSaga();
    }
  } catch (error) {
    yield put(setFetchedVisit_Failure("Unsuccessful post"));
  }
}
