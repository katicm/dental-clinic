import React, { Fragment, useEffect } from "react";
import { Link } from "react-router-dom";
import { HeaderContainer } from "./header.styles";
import { setSignOut } from "../../redux/auth/auth.action";
import { connect } from "react-redux";
import { Divider } from "antd";

const Header = ({ signOut, user }) => {
  useEffect(() => {
    if (user === null) localStorage.clear();
  }, [user]);

  return (
    <HeaderContainer>
      {user ? (
        <Fragment>
          <Link to="/account">
            <button>ACCOUNT</button>
          </Link>
          <Link to="/user">
            <button>USER</button>
          </Link>
          <Link to="/patient">
            <button>PATIENT</button>
          </Link>
          <Link to="/service">
            <button>SERVICE</button>
          </Link>
          <Link to="/department">
            <button>DEPARTMENT</button>
          </Link>
          <Link to="/doctor">
            <button>DOCTOR</button>
          </Link>
          <Link to="/appointment">
            <button>APPOINTMENT</button>
          </Link>
          <Link to="/treatment">
            <button>TREATMENT</button>
          </Link>
          <Link to="/visit">
            <button>VISIT</button>
          </Link>
          <Link to="/payment">
            <button>PAYMENT</button>
          </Link>
          <button style={{ color: "red" }} onClick={signOut}>
            Sign OUT
          </button>
        </Fragment>
      ) : (
        <Fragment>
          <Link to="/sign-in">SIGN IN</Link>
          <Divider type="vertical" />
          <Link to="/sign-up">SIGN UP</Link>
        </Fragment>
      )}
    </HeaderContainer>
  );
};
const mapDispatchToProps = (dispatch) => ({
  signOut: () => dispatch(setSignOut()),
});
const mapStateToProps = (state) => ({
  user: state.auth.user,
});
export default connect(mapStateToProps, mapDispatchToProps)(Header);
