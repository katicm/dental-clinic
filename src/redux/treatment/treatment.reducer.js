import TreatmentActionTypes from "./treatment.types";

const INITAL_STATE = {
  isLoading: true,
  error: null,
  data: [],
};

const treatmentReducer = (state = INITAL_STATE, action) => {
  switch (action.type) {
    case TreatmentActionTypes.FETCH_TREATMENT_START: {
      return { data: [], error: null, isLoading: true };
    }
    case TreatmentActionTypes.DELETE_TREATMENT_START:
    case TreatmentActionTypes.UPDATE_TREATMENT_START:
    case TreatmentActionTypes.CREATE_TREATMENT_START: {
      return { ...state, isLoading: true, error: null };
    }
    case TreatmentActionTypes.ADD_TREATMENT_START: {
      return {
        ...state,
        error: null,
        data: [action.item, ...state.data.filter((item) => item.key !== 0)],
      };
    }
    case TreatmentActionTypes.CANCEL_CREATE_TREATMENT: {
      return {
        ...state,
        error: null,
        data: state.data.filter((item) => item.key !== 0),
      };
    }
    case TreatmentActionTypes.SET_FETCHED_TREATMENT: {
      return { data: action.payload, isLoading: false, error: null };
    }
    case TreatmentActionTypes.SET_FETCHED_TREATMENT_FAILURE: {
      return { ...state, error: action.error, isLoading: false };
    }
    default:
      return state;
  }
};
export default treatmentReducer;
