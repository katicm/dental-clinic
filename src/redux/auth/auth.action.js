import AuthActionTypes from "./auth.types";

export const signIn = (data) => ({
  type: AuthActionTypes.SIGN_IN_START,
  data,
});

export const setSignIn = (payload) => ({
  type: AuthActionTypes.SET_SIGN_IN,
  payload,
});

export const setAuth_Failure = (error) => ({
  type: AuthActionTypes.SET_AUTH_FAILURE,
  error,
});

export const setSignOut = () => ({
  type: AuthActionTypes.SET_SIGN_OUT,
});

export const signUp = (data) => ({
  type: AuthActionTypes.SIGN_UP_START,
  data,
});

export const setSignUp = (payload) => ({
  type: AuthActionTypes.SET_SIGN_UP,
  payload,
});
