import AppointmentActionTypes from "./appointment.types";

const INITAL_STATE = {
  isLoading: true,
  error: null,
  data: [],
};

const appointmentReducer = (state = INITAL_STATE, action) => {
  switch (action.type) {
    case AppointmentActionTypes.FETCH_APPOINTMENT_START: {
      return { data: [], error: null, isLoading: true };
    }
    case AppointmentActionTypes.DELETE_APPOINTMENT_START:
    case AppointmentActionTypes.UPDATE_APPOINTMENT_START:
    case AppointmentActionTypes.CREATE_APPOINTMENT_START: {
      return { ...state, isLoading: true, error: null };
    }
    case AppointmentActionTypes.ADD_APPOINTMENT_START: {
      return {
        ...state,
        error: null,
        data: [action.item, ...state.data.filter((item) => item.key !== 0)],
      };
    }
    case AppointmentActionTypes.CANCEL_CREATE_APPOINTMENT: {
      return {
        ...state,
        error: null,
        data: state.data.filter((item) => item.key !== 0),
      };
    }
    case AppointmentActionTypes.SET_FETCHED_APPOINTMENT: {
      return { data: action.payload, isLoading: false, error: null };
    }
    case AppointmentActionTypes.SET_FETCHED_APPOINTMENT_FAILURE: {
      return { ...state, error: action.error, isLoading: false };
    }
    default:
      return state;
  }
};
export default appointmentReducer;
