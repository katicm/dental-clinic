import React, { useState, useEffect } from "react";
import { Form, Input, Button, Spin, Divider } from "antd";
import {
  UserOutlined,
  LockOutlined,
  SmileOutlined,
  PhoneOutlined,
} from "@ant-design/icons";
import { signUp } from "../../redux/auth/auth.action";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import "./sign.style.scss";

const SignUpForm = (props) => {
  const [form] = Form.useForm();
  const [redirect, setRedirect] = useState();
  const { signUp, auth } = props;
  const { isLoading, error } = auth;

  const onFinish = (data) => {
    signUp(data);
  };

  useEffect(() => {
    if (error) {
      form.resetFields();
    }
  }, [error, form]);

  useEffect(() => {
    if (auth.data) {
      setRedirect("/sign-in");
    }
  }, [auth]);

  useEffect(() => {
    setRedirect();
  }, []);

  const handleSignIn = () => {
    setRedirect("/sign-in");
  };

  if (redirect) return <Redirect to={redirect} />;
  return (
    <div>
      <Spin spinning={isLoading} size="large">
        <Form
          form={form}
          name="normal_login"
          className="register-form"
          onFinish={onFinish}
        >
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                message: "Please input your Email!",
              },
              {
                type: "email",
                message: "The input is not valid E-mail!",
              },
            ]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Email"
            />
          </Form.Item>
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: "Please input your Username!",
              },
            ]}
          >
            <Input
              prefix={<SmileOutlined className="site-form-item-icon" />}
              placeholder="Username"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your Password!",
              },
            ]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item
            name="phone"
            rules={[
              {
                required: true,
                message: "Please input your Phone!",
              },
            ]}
          >
            <Input
              prefix={<PhoneOutlined className="site-form-item-icon" />}
              placeholder="Phone"
            />
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              REGISTER
            </Button>
          </Form.Item>
          <Divider>OR</Divider>
          <Form.Item>
            Already a member?
            <div className="link-element" onClick={handleSignIn}>
              Sign in
            </div>
          </Form.Item>
        </Form>
      </Spin>
    </div>
  );
};
const mapDispatchToProps = (dispatch) => ({
  signUp: (data) => dispatch(signUp(data)),
});
const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps, mapDispatchToProps)(SignUpForm);
