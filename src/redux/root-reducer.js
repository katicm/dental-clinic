import { combineReducers } from "redux";
import AuthActionTypes from "../redux/auth/auth.types";

import userReducer from "./user/user.reducer";
import accountReducer from "./account/account.reducer";
import authReducer from "./auth/auth.reducer";
import patientReducer from "./patient/patient.reducer";
import serviceReducer from "./service/service.reducer";
import departmentReducer from "./department/department.reducer";
import doctorReducer from "./doctor/doctor.reducer";
import appointmentReducer from "./appointment/appointment.reducer";
import treatmentReducer from "./treatment/treatment.reducer";
import visitReducer from "./visit/visit.reducer";
import paymentReducer from "./payment/payment.reducer";

const applicationReducers = combineReducers({
  user: userReducer,
  account: accountReducer,
  auth: authReducer,
  patient: patientReducer,
  service: serviceReducer,
  department: departmentReducer,
  doctor: doctorReducer,
  appointment: appointmentReducer,
  treatment: treatmentReducer,
  visit: visitReducer,
  payment: paymentReducer,
});
const rootReducer = (state, action) => {
  if (action.type === AuthActionTypes.SET_SIGN_OUT) state = undefined;
  return applicationReducers(state, action);
};

export default rootReducer;
