import { takeEvery } from "redux-saga/effects";
import {
  fetchAccountSaga,
  deleteAccountSaga,
  updateAccountSaga,
  createAccountSaga,
} from "./account/account.saga";
import {
  fetchUserSaga,
  deleteUserSaga,
  updateUserSaga,
  createUserSaga,
} from "./user/user.saga";
import {
  fetchPatientSaga,
  deletePatientSaga,
  updatePatientSaga,
  createPatientSaga,
} from "./patient/patient.saga";
import {
  fetchServiceSaga,
  deleteServiceSaga,
  updateServiceSaga,
  createServiceSaga,
} from "./service/service.saga";
import {
  fetchDepartmentSaga,
  deleteDepartmentSaga,
  updateDepartmentSaga,
  createDepartmentSaga,
} from "./department/department.saga";
import {
  fetchDoctorSaga,
  deleteDoctorSaga,
  updateDoctorSaga,
  createDoctorSaga,
} from "./doctor/doctor.saga";
import {
  fetchAppointmentSaga,
  deleteAppointmentSaga,
  updateAppointmentSaga,
  createAppointmentSaga,
} from "./appointment/appointment.saga";
import {
  fetchTreatmentSaga,
  deleteTreatmentSaga,
  updateTreatmentSaga,
  createTreatmentSaga,
} from "./treatment/treatment.saga";
import {
  fetchVisitSaga,
  deleteVisitSaga,
  updateVisitSaga,
  createVisitSaga,
} from "./visit/visit.saga";
import {
  fetchPaymentSaga,
  deletePaymentSaga,
  updatePaymentSaga,
  createPaymentSaga,
} from "./payment/payment.saga";
import { signInSaga, signUpSaga } from "./auth/auth.saga";
import AccountActionTypes from "./account/account.types";
import UserActionTypes from "./user/user.types";
import AuthActionTypes from "./auth/auth.types";
import PatientActionTypes from "./patient/patient.types";
import ServiceActionTypes from "./service/service.types";
import DepartmentActionTypes from "./department/department.types";
import DoctorActionTypes from "./doctor/doctor.types";
import AppointmentActionTypes from "./appointment/appointment.types";
import TreatmentActionTypes from "./treatment/treatment.types";
import VisitActionTypes from "./visit/visit.types";
import PaymentActionTypes from "./payment/payment.types";

export const auth = () => ({
  headers: {
    Authorization: "Basic " + localStorage.getItem("basic"),
  },
});

export function* watchAccount() {
  yield takeEvery(AccountActionTypes.FETCH_ACCOUNT_START, fetchAccountSaga);
  yield takeEvery(AccountActionTypes.DELETE_ACCOUNT_START, deleteAccountSaga);
  yield takeEvery(AccountActionTypes.UPDATE_ACCOUNT_START, updateAccountSaga);
  yield takeEvery(AccountActionTypes.CREATE_ACCOUNT_START, createAccountSaga);
}
export function* watchUser() {
  yield takeEvery(UserActionTypes.FETCH_USER_START, fetchUserSaga);
  yield takeEvery(UserActionTypes.DELETE_USER_START, deleteUserSaga);
  yield takeEvery(UserActionTypes.UPDATE_USER_START, updateUserSaga);
  yield takeEvery(UserActionTypes.CREATE_USER_START, createUserSaga);
}
export function* watchPatient() {
  yield takeEvery(PatientActionTypes.FETCH_PATIENT_START, fetchPatientSaga);
  yield takeEvery(PatientActionTypes.DELETE_PATIENT_START, deletePatientSaga);
  yield takeEvery(PatientActionTypes.UPDATE_PATIENT_START, updatePatientSaga);
  yield takeEvery(PatientActionTypes.CREATE_PATIENT_START, createPatientSaga);
}
export function* watchService() {
  yield takeEvery(ServiceActionTypes.FETCH_SERVICE_START, fetchServiceSaga);
  yield takeEvery(ServiceActionTypes.DELETE_SERVICE_START, deleteServiceSaga);
  yield takeEvery(ServiceActionTypes.UPDATE_SERVICE_START, updateServiceSaga);
  yield takeEvery(ServiceActionTypes.CREATE_SERVICE_START, createServiceSaga);
}
export function* watchDepartment() {
  yield takeEvery(
    DepartmentActionTypes.FETCH_DEPARTMENT_START,
    fetchDepartmentSaga
  );
  yield takeEvery(
    DepartmentActionTypes.DELETE_DEPARTMENT_START,
    deleteDepartmentSaga
  );
  yield takeEvery(
    DepartmentActionTypes.UPDATE_DEPARTMENT_START,
    updateDepartmentSaga
  );
  yield takeEvery(
    DepartmentActionTypes.CREATE_DEPARTMENT_START,
    createDepartmentSaga
  );
}
export function* watchDoctor() {
  yield takeEvery(DoctorActionTypes.FETCH_DOCTOR_START, fetchDoctorSaga);
  yield takeEvery(DoctorActionTypes.DELETE_DOCTOR_START, deleteDoctorSaga);
  yield takeEvery(DoctorActionTypes.UPDATE_DOCTOR_START, updateDoctorSaga);
  yield takeEvery(DoctorActionTypes.CREATE_DOCTOR_START, createDoctorSaga);
}
export function* watchAppointment() {
  yield takeEvery(
    AppointmentActionTypes.FETCH_APPOINTMENT_START,
    fetchAppointmentSaga
  );
  yield takeEvery(
    AppointmentActionTypes.DELETE_APPOINTMENT_START,
    deleteAppointmentSaga
  );
  yield takeEvery(
    AppointmentActionTypes.UPDATE_APPOINTMENT_START,
    updateAppointmentSaga
  );
  yield takeEvery(
    AppointmentActionTypes.CREATE_APPOINTMENT_START,
    createAppointmentSaga
  );
}
export function* watchTreatment() {
  yield takeEvery(
    TreatmentActionTypes.FETCH_TREATMENT_START,
    fetchTreatmentSaga
  );
  yield takeEvery(
    TreatmentActionTypes.DELETE_TREATMENT_START,
    deleteTreatmentSaga
  );
  yield takeEvery(
    TreatmentActionTypes.UPDATE_TREATMENT_START,
    updateTreatmentSaga
  );
  yield takeEvery(
    TreatmentActionTypes.CREATE_TREATMENT_START,
    createTreatmentSaga
  );
}
export function* watchVisit() {
  yield takeEvery(VisitActionTypes.FETCH_VISIT_START, fetchVisitSaga);
  yield takeEvery(VisitActionTypes.DELETE_VISIT_START, deleteVisitSaga);
  yield takeEvery(VisitActionTypes.UPDATE_VISIT_START, updateVisitSaga);
  yield takeEvery(VisitActionTypes.CREATE_VISIT_START, createVisitSaga);
}
export function* watchPayment() {
  yield takeEvery(PaymentActionTypes.FETCH_PAYMENT_START, fetchPaymentSaga);
  yield takeEvery(PaymentActionTypes.DELETE_PAYMENT_START, deletePaymentSaga);
  yield takeEvery(PaymentActionTypes.UPDATE_PAYMENT_START, updatePaymentSaga);
  yield takeEvery(PaymentActionTypes.CREATE_PAYMENT_START, createPaymentSaga);
}
export function* watchAuth() {
  yield takeEvery(AuthActionTypes.SIGN_IN_START, signInSaga);
  yield takeEvery(AuthActionTypes.SIGN_UP_START, signUpSaga);
}
