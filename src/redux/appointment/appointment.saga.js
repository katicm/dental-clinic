import { put } from "redux-saga/effects";
import {
  setFetchedAppointment,
  setFetchedAppointment_Failure,
} from "./appointment.actions";
import axios from "axios";
import { auth } from "../root-saga";

export function* fetchAppointmentSaga() {
  try {
    const response = yield axios.get(
      `${process.env.REACT_APP_BASE_ANYPOINT}/appointment`,
      auth()
    );

    yield put(
      setFetchedAppointment(
        response.data.map((item) => ({ ...item, key: item.appointmentID }))
      )
    );
  } catch (error) {
    yield put(setFetchedAppointment_Failure(error));
  }
}
export function* deleteAppointmentSaga(action) {
  try {
    const response = yield axios.delete(
      `${process.env.REACT_APP_BASE_ANYPOINT}/appointment/${action.id}`,
      auth()
    );
    if (response.status === 204) {
      yield fetchAppointmentSaga();
    }
  } catch (error) {
    yield put(
      setFetchedAppointment_Failure(
        "Delete statment conflicted with reference constraint"
      )
    );
  }
}
export function* updateAppointmentSaga(action) {
  try {
    const response = yield axios.put(
      `${process.env.REACT_APP_BASE_ANYPOINT}/appointment/${action.item.appointmentID}`,
      action.item,
      auth()
    );
    if (response.status === 200) {
      yield fetchAppointmentSaga();
    }
  } catch (error) {
    yield put(setFetchedAppointment_Failure("Unsuccessful update"));
  }
}
export function* createAppointmentSaga(action) {
  try {
    const response = yield axios.post(
      `${process.env.REACT_APP_BASE_ANYPOINT}/appointment`,
      action.item,
      auth()
    );
    if (response.status === 201) {
      yield fetchAppointmentSaga();
    }
  } catch (error) {
    yield put(setFetchedAppointment_Failure("Unsuccessful post"));
  }
}
