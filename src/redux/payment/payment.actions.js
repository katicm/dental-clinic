import PaymentActionTypes from "./payment.types";

export const fetchPayment = () => ({
  type: PaymentActionTypes.FETCH_PAYMENT_START,
});

export const deletePayment = (id) => ({
  type: PaymentActionTypes.DELETE_PAYMENT_START,
  id,
});

export const updatePayment = (item) => ({
  type: PaymentActionTypes.UPDATE_PAYMENT_START,
  item,
});

export const addPayment = (item) => ({
  type: PaymentActionTypes.ADD_PAYMENT_START,
  item,
});

export const createPayment = (item) => ({
  type: PaymentActionTypes.CREATE_PAYMENT_START,
  item,
});

export const cancelPayment = () => ({
  type: PaymentActionTypes.CANCEL_CREATE_PAYMENT,
});

export const setFetchedPayment = (data) => ({
  type: PaymentActionTypes.SET_FETCHED_PAYMENT,
  payload: data,
});

export const setFetchedPayment_Failure = (error) => ({
  type: PaymentActionTypes.SET_FETCHED_PAYMENT_FAILURE,
  error,
});
