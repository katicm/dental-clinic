import React from "react";
import useEditableTable from "../custom-table/useEditableTable";
import useOperationColumn from "../custom-table/useOperationColumn";
import Addable from "../custom-table/Addable";
import EditableTable from "../custom-table/EditableTable";
import moment from "moment";
import { connect } from "react-redux";
import {
  fetchVisit,
  deleteVisit,
  updateVisit,
  createVisit,
  cancelVisit,
  addVisit,
} from "../../redux/visit/visit.actions";
import "../custom-table/table.style.scss";

const VisitTable = (props) => {
  const {
    fetchVisit,
    deleteVisit,
    updateVisit,
    createVisit,
    cancelVisit,
    addVisit,
    visit,
  } = props;

  const { isLoading } = visit;

  const [getEditDelete, getSaveCancel] = useOperationColumn();

  const handleAddable = () => {
    addVisit({
      key: 0,
      datetime: moment().format("YYYY-MM-DDTHH:mm:ss"),
    });
    handleAdd();
  };

  const columns = [
    {
      title: "Visit ID",
      dataIndex: "visitID",
      width: "12%",
      sorter: (a, b) => a.visitID - b.visitID,
    },

    {
      title: "Appointment ID",
      dataIndex: "appointmentID",
      inputType: "number",
      editable: true,
      width: "12%",
      sorter: (a, b) => a.appointmentID - b.appointmentID,
    },
    {
      title: "Patient ID",
      dataIndex: "patientID",
      inputType: "number",
      editable: true,
      width: "12%",
      sorter: (a, b) => a.patientID - b.patientID,
    },
    {
      title: "Doctor ID",
      dataIndex: "doctorID",
      inputType: "number",
      editable: true,
      width: "12%",
      sorter: (a, b) => a.doctorID - b.doctorID,
    },
    {
      title: "Payment ID",
      dataIndex: "paymentID",
      inputType: "number",
      editable: true,
      width: "12%",
      sorter: (a, b) => a.paymentID - b.paymentID,
    },
    {
      title: <center>Visiting Date</center>,
      dataIndex: "datetime",
      editable: true,
      width: "15%",
      inputType: "dateTime",
      sorter: (a, b) => {
        if (a.datetime < b.datetime) {
          return -1;
        } else if (a.datetime > b.datetime) {
          return 1;
        }
        return 0;
      },
      render: (date) => date.replace("T", " "),
    },
    {
      title: <center>Operation</center>,
      dataIndex: "operation",
      width: "10%",
      render: (_, record) => {
        const editable = record.key === editingKey ? true : false;
        return editable
          ? getSaveCancel(handleSave, handleCancel)
          : getEditDelete(editingKey, record, handleEdit, handleDelete);
      },
    },
  ];

  const [
    form,
    handleSave,
    handleCancel,
    handleEdit,
    handleDelete,
    handleAdd,
    editingKey,
    mergedColumns,
    handleChange,
    currentPage,
  ] = useEditableTable(
    columns,
    fetchVisit,
    deleteVisit,
    updateVisit,
    createVisit,
    cancelVisit,
    visit
  );
  return (
    <React.Fragment>
      <Addable handleAddable={handleAddable} />
      <EditableTable
        form={form}
        isLoading={isLoading}
        data={visit.data}
        columns={mergedColumns}
        onChange={handleChange}
        currentPage={currentPage}
      />
    </React.Fragment>
  );
};
const mapStateToProps = (state) => ({
  visit: state.visit,
});
const mapDispatchToProps = (dispatch) => ({
  fetchVisit: () => dispatch(fetchVisit()),
  deleteVisit: (id) => dispatch(deleteVisit(id)),
  updateVisit: (item) => dispatch(updateVisit(item)),
  createVisit: (item) => dispatch(createVisit(item)),
  cancelVisit: () => dispatch(cancelVisit()),
  addVisit: (nowDate) => dispatch(addVisit(nowDate)),
});
export default connect(mapStateToProps, mapDispatchToProps)(VisitTable);
