import React from "react";
import useEditableTable from "../custom-table/useEditableTable";
import useColumnSearch from "../custom-table/useColumnSearch";
import useOperationColumn from "../custom-table/useOperationColumn";
import Addable from "../custom-table/Addable";
import EditableTable from "../custom-table/EditableTable";
import moment from "moment";
import { connect } from "react-redux";
import {
  fetchAppointment,
  deleteAppointment,
  updateAppointment,
  createAppointment,
  cancelAppointment,
  addAppointment,
} from "../../redux/appointment/appointment.actions";
import "../custom-table/table.style.scss";

const AppointmentTable = (props) => {
  const {
    fetchAppointment,
    deleteAppointment,
    updateAppointment,
    createAppointment,
    cancelAppointment,
    addAppointment,
    appointment,
  } = props;

  const { isLoading } = appointment;

  const [getColumnSearchProps] = useColumnSearch();
  const [getEditDelete, getSaveCancel] = useOperationColumn();

  const handleAddable = () => {
    addAppointment({
      key: 0,
      scheduledDate: moment().format("YYYY-MM-DDTHH:mm:ss"),
    });
    handleAdd();
  };

  const columns = [
    {
      title: "Appointment ID",
      dataIndex: "appointmentID",
      width: "12%",
      sorter: (a, b) => a.appointmentID - b.appointmentID,
    },

    {
      title: "Patient ID",
      dataIndex: "patientID",
      inputType: "number",
      editable: true,
      width: "12%",
      sorter: (a, b) => a.patientID - b.patientID,
    },
    {
      title: "Doctor ID",
      dataIndex: "doctorID",
      inputType: "number",
      editable: true,
      width: "12%",
      sorter: (a, b) => a.doctorID - b.doctorID,
    },
    {
      title: <center>Scheduled Date</center>,
      dataIndex: "scheduledDate",
      editable: true,
      width: "15%",
      inputType: "dateTime",
      sorter: (a, b) => {
        if (a.scheduledDate < b.scheduledDate) {
          return -1;
        } else if (a.scheduledDate > b.scheduledDate) {
          return 1;
        }
        return 0;
      },
      render: (date) => date.replace("T", " "),
    },
    {
      title: <center>Notes</center>,
      dataIndex: "notes",
      editable: true,
      width: "13%",
      ...getColumnSearchProps("notes"),
    },
    {
      title: <center>Operation</center>,
      dataIndex: "operation",
      width: "10%",
      render: (_, record) => {
        const editable = record.key === editingKey ? true : false;
        return editable
          ? getSaveCancel(handleSave, handleCancel)
          : getEditDelete(editingKey, record, handleEdit, handleDelete);
      },
    },
  ];

  const [
    form,
    handleSave,
    handleCancel,
    handleEdit,
    handleDelete,
    handleAdd,
    editingKey,
    mergedColumns,
    handleChange,
    currentPage,
  ] = useEditableTable(
    columns,
    fetchAppointment,
    deleteAppointment,
    updateAppointment,
    createAppointment,
    cancelAppointment,
    appointment
  );
  return (
    <React.Fragment>
      <Addable handleAddable={handleAddable} />
      <EditableTable
        form={form}
        isLoading={isLoading}
        data={appointment.data}
        columns={mergedColumns}
        onChange={handleChange}
        currentPage={currentPage}
      />
    </React.Fragment>
  );
};
const mapStateToProps = (state) => ({
  appointment: state.appointment,
});
const mapDispatchToProps = (dispatch) => ({
  fetchAppointment: () => dispatch(fetchAppointment()),
  deleteAppointment: (id) => dispatch(deleteAppointment(id)),
  updateAppointment: (item) => dispatch(updateAppointment(item)),
  createAppointment: (item) => dispatch(createAppointment(item)),
  cancelAppointment: () => dispatch(cancelAppointment()),
  addAppointment: (nowDate) => dispatch(addAppointment(nowDate)),
});
export default connect(mapStateToProps, mapDispatchToProps)(AppointmentTable);
