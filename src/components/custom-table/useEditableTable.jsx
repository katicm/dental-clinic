import { useState, useEffect } from "react";
import { Form, message } from "antd";

message.config({
  maxCount: 1,
});

const useEditableTable = (
  columns,
  fetchData,
  deleteRow,
  updateRow,
  createRow,
  cancelAdd,
  store
) => {
  const [form] = Form.useForm();
  const [currentPage, setCurrentPage] = useState(1);
  const [editingKey, setEditingKey] = useState("");
  const { error, data } = store;

  useEffect(() => {
    const fetch = () => {
      fetchData();
    };
    fetch();
  }, [fetchData]);

  useEffect(() => {
    if (data.length)
      if (data[0].key !== 0) {
        message.success("Successful!", 1);
        setEditingKey("");
      }
  }, [data]);

  useEffect(() => {
    if (error) {
      if (data.length) {
        if (data[0].key === 0) setEditingKey(0);
      } else message.error("Unsuccessful!", 2);
    }
  }, [error, data]);

  const handleEdit = (record) => {
    form.setFieldsValue({ ...record });
    setEditingKey(record.key);
  };
  const handleCancel = () => {
    cancelAdd();
    setEditingKey("");
  };
  const handleAdd = () => {
    form.resetFields();
    setCurrentPage(1);
    setEditingKey(0);
  };
  const handleChange = (page) => {
    setCurrentPage(page);
    handleCancel();
  };
  const handleDelete = (key) => {
    deleteRow(key);
  };
  const handleSave = () => {
    const row = form.getFieldValue();
    if (row.key) {
      updateRow(row);
    } else {
      createRow(row);
    }
  };
  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record) => {
        return {
          record,
          inputType: col.inputType,
          dataIndex: col.dataIndex,
          title: col.title,
          editing: record.key === editingKey ? true : false,
          form,
        };
      },
    };
  });

  return [
    form,
    handleSave,
    handleCancel,
    handleEdit,
    handleDelete,
    handleAdd,
    editingKey,
    mergedColumns,
    handleChange,
    currentPage,
  ];
};
export default useEditableTable;
